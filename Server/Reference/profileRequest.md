{
  user(id: "chilledcow") {
    display_name
    id
    href
    images {
			url
      width
      height
    }
    playlists {
      name
      uri
      href
      id
      images {
        url
        width
        height
      }      
      description
      tracks {
        track {
          name
          id
          uri
          preview_url
          artists {
            name
            id
          }          
        }
      }
    }
  }
}



