// TODO: check for locale

let locale = 'en';
try{
    locale = getQueryParam('locale');
} catch(error){
    
}

var notifications = {};

var request = new XMLHttpRequest();
request.open('GET', `./translations/${locale}.json`, false);  // `false` makes the request synchronous
request.send(null);

if (request.status === 200) {
    // log(request, "Translations Recieved", 'Debug',1);
    notifications = JSON.parse(request.responseText);
    log(notifications, "Notifications", 'Debug',1);
} else {
    notifications = {
        "assignUserToTicket":{
            "failure":"Unable to assign Ticket"
        },
        "ongingCallTicket":{
            "failure":"Unable to get data of ongoing call"
        },
        "get":{
            "customFields":{
                "failure":"Unable to get list of assigned custom fields"
            },
            "loggedInUser":{
                "failure":"Unable to get logged in user"
            },
            "ticket":{
                "failure":"Unable to get ticket information"
            },
            "getCustomerInfosForCustomerId":{
                "failure":"Unable to get customer information"
            }
        },
        "compute":{
            "failure":"Unable to call faceMatch API"
        }, 
        "livelinessCheck":{
            "failure":"Unable to update liveliness-check status",
            "question":{
                "failure":"Unable to create liveliness questions"
            }
        },
        "faceMatch":{
            "failure":"Unable to update face-match result"
        },
        "finalVerification":{
            "failure":"Unable to change Ticket State"
        },
        "takeSnap":{
            "failure":"Failed to capture Image"
        },
        "addOrUpdateMessage":{
            "failure":"Failed to add Attachment"
        },
        "applicationVerification":{
            "name":{
                "failure":"Failed to update"
            },        
            "address":{
                "failure":"Failed to update"
            },        
            "DOB":{
                "failure":"Failed to update"
            },
            "all":{
                "failure":"Failed to update"
            }
        },
        "panData":{
            "failure":"Information can not be captured at this moment"
        }
    };
    log(notifications, "Failed to get Notifications set with Defaults", 'Debug');
}