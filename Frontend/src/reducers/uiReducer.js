const uiReducer = (state = {}, action ) => {
    switch(action.type){
            
        case 'SET_LOADED':
            return{
                ...state,
                loaded: action.data
            }

        default : 
            return state;
    }
}

export default uiReducer