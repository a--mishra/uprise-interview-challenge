import React from 'react'
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import ShowCampaign from '../showcampaign/showcampaign'
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';



const useStyles = makeStyles(theme => ({

  formControl: {
    minWidth: 215, 
    height:35
  },
  option: {
    fontSize:12
  },
  
  setBorder: {
    height:62,
    marginLeft:20
  },
  labelsize: {
      fontSize:10
  },
  optionsize: {
    fontSize: 12
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  selectheight: {
    height:62,
  },
  optionHeight: {
    margin:5
  }  
}));

const optionsList = [
  { id: '1', title: 'Campaign 1'},
  { id: '2', title: 'Campaign 2' },
  { id: '3', title: 'Campaign 3' },
  { id: '4', title: 'Campaign 4'},
  { id: '5', title: 'Campaign 5'},
  { id: '6', title: 'Campaign 6'},
  { id: '7', title: 'Campaign 7'},
  { id: '8', title: 'Campaign 8'},
  { id: '9', title: 'Campaign 9'},
  { id: '10', title: 'Campaign 10'}
];

export default function SelectCampaign(){
  // const [value, setValue] = React.useState({
  //   value: '',
  // });
  const classes = useStyles();
   
  const defaultProps = {
    options: optionsList,
    getOptionLabel: option => option.title,
  
  };

  const flatProps = {
    options: optionsList.map(option => option.title),
  };

  const [value, setValue] = React.useState({
    id:''
  });

  const onCampaignChange= (event, values) => {
  
    if(values != null)
    {
      setValue({
        id:values.id
      })
    }
  
  } 

  

  return (
    <div>
    <div className={classes.setBorder} >
        <FormControl className={classes.formControl}>
        <Autocomplete
        {...defaultProps}
        id="clear-on-escape"
        className="selectCampaign"
        classes={{
          option: classes.option,
        }}
        onChange={onCampaignChange}
        renderInput={params => (
          <TextField {...params} label="Select Campaign *" margin="normal" fullWidth  />
        )}
      />
      </FormControl>
      </div>
      <ShowCampaign value={value.id}/>
    </div>
    
  );
}
