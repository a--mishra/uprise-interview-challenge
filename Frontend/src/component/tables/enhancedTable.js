import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import QuotaSelect from '../quotaSelect/QuotaSelect';
import StatusSelect from '../statusSelect/StatusSelect'
import { lineHeight } from '@material-ui/system';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { setTimeout } from 'timers';
import DispositionSelect from '../DispositionSelect';
import CopyAll from '../CopyAll';


const columns = [
  { id: 'name', label: 'Name', minWidth: 170 },
  { id: 'disposition', label: 'Disposition', minWidth: 100 },
  {
    id: 'quota',
    label: 'Quota',
    minWidth: 170,
    align: 'left',
  },
  {
    id: 'status',
    label: 'Status',
    minWidth: 170,
    align: 'left',
  },
  {
    id: 'action',
    label: 'Action',
    minWidth: 170,
    align: 'left',
  },
];



function storeDatainTable(name, disposition, quota, status, action) {

  return { name, disposition, quota, status, action }
}

const tableData = [
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />), <CopyAll />,
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),
  storeDatainTable('Mumbai_Leads', <DispositionSelect />, <QuotaSelect />, <StatusSelect />, <CopyAll />),

]

var tableCustomHeight = 0;
var innerHeight = window.innerHeight;
const findheight = () => {
  setTimeout(() => {

    let footerHeight = 55;
    const topHeight = document.getElementById('tableheight').getBoundingClientRect().top;
    tableCustomHeight = innerHeight - (topHeight + footerHeight);
    document.getElementById('tableheight').style.height = tableCustomHeight + "px";

  }, 0)

}




const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    boxShadow: 'none'
  },
  tableWrapper: {
    height: tableCustomHeight,
    overflow: 'auto',
  },
  tablehead: {
    height: 36
  },

  tableRow: {
    height: 36,
    padding: '0 0 0 20px',
    lineHeight: '14px',
    fontSize: 12,
    color: '#333333'
  },
  headerColor: {
    backgroundColor: '#182C4C',
    color: '#F2F2F2',
    fontSize: '12px',
    fontWeight: 700,
    padding: '10px 0 10px 20px',
    lineHeight: '15px'
  },
  emptydata: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    fontSize: 12
  },
  pagination: {
    border: '1px solid #cccccc',
    boxShadow: '0 -2px 4px 0 #cccccc',
    minHeight: '60'
  },
  mainheader: {
    borderTop: '1px solid #cccccc'
  },
  searchbox: {
    margin: "10px 0 10px 20px"
  },

  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    display: 'inline-block',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },

    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  svgsize: {
    // width: theme.spacing(7),
    // height: '100%',
    // position: 'absolute',
    // pointerEvents: 'none',
    // display: 'flex',
    // alignItems: 'center',
    // justifyContent: 'center',
    // right:'0',
    '& > svg': {
      height: 9,
      width: 9
    },
  },
  searchIcon: {
    display: 'inline-block',
    border: '1px solid #ccc',
    borderRadius: 25,
  },

  inputRoot: {
    border: '1px solid #cccccc',
    borderRadius: '25px'
  },
  inputInput: {
    padding: theme.spacing(1, 7, 1, 1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },

  lebelsize: {
    fontSize: 10
  },
  serchbutton: {
    width: 160,
    height: 24,
    fontSize: 10
  },
  textfildfontsize: {
    '& ..MuiInputBase-input': {
      fontSize: 10
    },
  },

}));

export default function EnhancedTable(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(15);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  if (tableData.length !== 0) {
    return (

      <div className={classes.mainheader} >
        <div className={classes.searchbox}>

          <span className="leadlist">Leads list (2)</span>
          <div className={classes.searchIcon}>

            <TextField id="standard-basic" placeholder="Search" className="removeborder" InputProps={{
              disableUnderline: true,
              style: { fontSize: 10 }
            }} />
            <Button className="btnsize"
              startIcon={<SearchIcon style={{ fontSize: 14 }} />}
            >
            </Button>
          </div>
        </div>
        <Paper className={classes.root} >

          <div className={classes.tableWrapper} id="tableheight">
            <Table stickyHeader aria-label="sticky table">
              <TableHead className={classes.tablehead}>
                <TableRow>
                  {columns.map(column => (
                    <TableCell className={classes.headerColor}
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {tableData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                      {columns.map(column => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align} className={classes.tableRow}>
                            {value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </div>
          <TablePagination
            className={classes.pagination}
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={tableData.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </Paper>
        {findheight()}
      </div>

    );
  }
  else {

    return (

      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <div className={classes.emptydata}>
            <p> No Record to display, please create one by hitting the Add button!</p>
          </div>
        </div>
      </Paper>
    )
  }


}
