
function TableHeaders(data) {
let headers = {
    Start : [
        {
            id: 'id',
            label: '',
            minWidth: 1,
            align: 'center',
            searchable: true
        },
        {
            id: 'typeIcon',
            label: 'Type',
            minWidth: 50,
            align: 'center',
            searchable: true
        },
        {
            id: 'name',
            label: 'Name',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'meta',
            label: 'Meta Data',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'description',
            label: 'Description',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'action',
            label: 'Action',
            minWidth: 180,
            align: 'center',
            searchable: true
        },
    ],
    Sound : [
        {
            id: 'id',
            label: '',
            minWidth: 1,
            align: 'center',
            searchable: true
        },
        {
            id: 'typeIcon',
            label: 'Type',
            minWidth: 50,
            align: 'center',
            searchable: true
        },
        {
            id: 'name',
            label: 'Name',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'meta',
            label: 'Meta Data',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'description',
            label: 'Description',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'action',
            label: 'Action',
            minWidth: 180,
            align: 'center',
            searchable: true
        },
    ],
    Menu : [
        {
            id: 'id',
            label: '',
            minWidth: 1,
            align: 'center',
            searchable: true
        },
        {
            id: 'extension',
            label: 'Extension',
            minWidth: 10,
            align: 'center',
            searchable: true
        },
        {
            id: 'typeIcon',
            label: 'Type',
            minWidth: 10,
            align: 'center',
            searchable: true
        },
        {
            id: 'name',
            label: 'Name',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'meta',
            label: 'Meta Data',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'description',
            label: 'Description',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'action',
            label: 'Action',
            minWidth: 160,
            align: 'center',
            searchable: true
        },
    ],
    Phone : [
        {
            id: 'id',
            label: '',
            minWidth: 1,
            align: 'center',
            searchable: true
        },
        {
            id: 'typeIcon',
            label: 'Type',
            minWidth: 50,
            align: 'center',
            searchable: true
        },
        {
            id: 'name',
            label: 'Name',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'meta',
            label: 'Meta Data',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'description',
            label: 'Description',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'action',
            label: 'Action',
            minWidth: 180,
            align: 'center',
            searchable: true
        },
    ],
    Group : [
        {
            id: 'id',
            label: '',
            minWidth: 1,
            align: 'center',
            searchable: true
        },
        {
            id: 'typeIcon',
            label: 'Type',
            minWidth: 50,
            align: 'center',
            searchable: true
        },
        {
            id: 'name',
            label: 'Name',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'meta',
            label: 'Meta Data',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'description',
            label: 'Description',
            minWidth: 100,
            align: 'center',
            searchable: true
        },
        {
            id: 'action',
            label: 'Action',
            minWidth: 180,
            align: 'center',
            searchable: true
        },
    ]
}

    return headers[data];
}


function settings(data){
    let iframeData = getIframeData();

    if(iframeData[data] != null && iframeData[data] != undefined && iframeData[data] != ''){
        if(data == 'user'){
            if(iframeData[data] == undefined) {
                return '313s42vojenpqgcakulzmxtoixp4';
            }
        }
        return iframeData[data];
    }

    let settings = window.globalSettings;
        return settings[data];
}


function getIframeData() {
	var urlObj ={}
	//userId=wallboard1&userName=wallboard1&sessionId=d702-5d72d37b-ses-wallboarduser-R9jT9txS-546&userType=Wallboard-User&iframeId=iframe_617548

	var url = window.location.search.substring(1);
	var vars = url.split("&");
	for (var i=0;i<vars.length;i++) {
		var param = vars[i].split("=");
		urlObj[param[0]] = param[1];
    }
    return urlObj;
}

/**
 * not used
 */

function getCurrentDate() {
    let date = new Date();
    let dateStr =
        date.getFullYear() + "-" +
        ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
        ("00" + date.getDate()).slice(-2);
    return dateStr;
}

function getCurrentHour(date) {
    date = new Date(date);
    var dateStr =
        date.getFullYear() + "-" +
        ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
        ("00" + date.getDate()).slice(-2) + " " +
        ("00" + date.getHours()).slice(-2) + ":" +"00"; 
    return dateStr;
}



// -----------------Iterate-----------------------------------------------

function reIndexIVRData(flowData) {
    Object.keys(flowData).forEach( (key,index) => {
        if(index != 0)
        path.pop();
        path.push(index+1)

        flowData[key].id = path.join('_');

        if(key == 'typeIcon' || key == 'action')
            flowData[key] = {};

        if(flowData[key].children != undefined){
            flowData[key].children = reIndexIVRData(flowData[key].children);
        }
        
        if(index+1 == flowData.length){
            path.pop();
        }
    })
    
    return flowData;
}

let path = [];

// ----------------- ADD -----------------------------------------------

function addDataAtId(nodeId, flowData, nodeData) {
    if(nodeId == '0'){
        flowData.push(nodeData);
        path = [];
        flowData = reIndexIVRData(flowData);
        return flowData;
    }

    Object.keys(flowData).forEach( (key,index) => {
        if(index != 0)
        path.pop();
        path.push(index+1)

        flowData[key].id = path.join('_');
        
        if(flowData[key].id == nodeId){
            if(flowData[key].children == undefined)
                flowData[key].children = []
            flowData[key].children.push(nodeData);
        }

        if(flowData[key].children != undefined){
            flowData[key].children = addDataAtId(nodeId, flowData[key].children, nodeData);
        }
        
        if(index+1 == flowData.length){
            path.pop();
        }
    })
    
    return flowData;
}

// -----------------DELETE-----------------------------------------------

function deleteDataAtId(nodeId, flowData) {
    

    Object.keys(flowData).forEach( (key,index) => {
        if(index != 0)
        path.pop();
        path.push(index+1)

        
        let temp = nodeId.split('_');
        let childIndex = temp[temp.length - 1] - 1;
        let parentPath = temp.length  == 1 ? null :temp.slice(0,temp.length - 1).join('_')

        if(parentPath == null){
            if(childIndex == index)
                flowData.splice(childIndex,1)
                path = [];
                flowData = reIndexIVRData(flowData);
                return flowData;
        } else {
            flowData[key].id = path.join('_');

            if(flowData[key].id == parentPath){
                if(flowData[key].children != undefined){
                    flowData[key].children.splice(childIndex,1)
                }
            }
    
            if(flowData[key].children != undefined){
                flowData[key].children = deleteDataAtId(nodeId, flowData[key].children);
            }
        }
        
        if(index+1 == flowData.length){
            path.pop();
        }

    })
    
    return flowData;
}


function editDataAtId(nodeId, flowData, nodeData) {

    Object.keys(flowData).forEach( (key,index) => {
        if(index != 0)
        path.pop();
        path.push(index+1)

        flowData[key].id = path.join('_');
        
        if(flowData[key].id == nodeId){

            if(flowData[key].children == undefined)
                flowData[key].children = []

            Object.keys(nodeData).forEach((nodeKey, nodeIndex)=>{
                flowData[key][nodeKey] = nodeData[nodeKey]
            })
            // flowData[key] = nodeData;
        }

        if(flowData[key].children != undefined){
            flowData[key].children = editDataAtId(nodeId, flowData[key].children, nodeData);
        }
        
        if(index+1 == flowData.length){
            path.pop();
        }
    })
    
    return flowData;
}

function getAvailiableExtensions(nodeId, flowData, addOrEdit){
    //console.log(JSON.stringify({nodeId, flowData}));
    let path = [];
    let extensions = ['1','2','3','4','5','6','7','8','9','0'];
    let tempNodeId = nodeId;

    if(addOrEdit == 'Edit'){        
        nodeId = nodeId.split('_');
        nodeId.pop();
        nodeId = nodeId.join('_');
    }

    function reIndexIVRData1(flowData) {
        for(let i=0; i<flowData.length; i++) {
            let index = i;
            if(index != 0)
            path.pop();
            path.push(index+1)
    
            flowData[index].id = path.join('_');
    
            flowData[index].typeIcon = {};
            flowData[index].action = {};
        
            if(flowData[index].id == nodeId){

                if(flowData[index].children == undefined)
                    flowData[index].children = []
    
                let nodeData = flowData[index].children;
                Object.keys(nodeData).forEach((nodeKey, nodeIndex)=>{
                    if(nodeData[nodeKey].id == tempNodeId){
                        // do nothing;
                    } else{
                        if(nodeData[nodeKey].extension == ''){
                            // do nothing
                        } else {
                            extensions.splice(extensions.indexOf(nodeData[nodeKey].extension), 1)
                        }
                    }
                })
            }

            if(flowData[index].children != undefined){
                flowData[index].children = reIndexIVRData1(flowData[index].children);
            }
            
            if(index+1 == flowData.length){
                path.pop();
            }
        }
        
        return flowData;
    }

    reIndexIVRData1(flowData);
    return extensions;
}

var firstTimeRender = true

function getGroupName(data, id){
    let rdata = '';
    Object.keys(data).forEach( (key, index)=>{
        let obj = data[key];
        if(obj.id == id)
            rdata = obj.name
    })
   return rdata;
}