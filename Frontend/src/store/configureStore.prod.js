import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import {rootReducer} from '../reducers/rootReducer'
import { BrowserRouter } from 'react-router-dom'
import { routerMiddleware} from 'react-router-redux'
import { devToolsEnhancer,composeWithDevTools } from 'redux-devtools-extension'

const composeEnhancers = ''//window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

const configureStore = preloadedState => {
    const store = createStore(
                    rootReducer,
                    preloadedState,
                    compose(
                        composeWithDevTools(applyMiddleware(thunk,createLogger) )
                    )
                )
    return store
}
export default configureStore

