import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


const buttonStyles = theme => ({
    root: {
        backgroundColor: '#f2f2f2',
        color: '#999',
        border: '1px solid #f2f2f2',
        fontSize: '0.857rem',
        borderRadius: '18px',
        boxShadow: 'none',
        textTransform: 'none',
		height: '24px',
		minWidth: '24px',
        lineHeight: '20px',
		padding: '0',
		
    }
});


export class CustomIconButton extends Component{
	constructor(props){
        super(props)
    }
	render(){
		const { children, classes, onClick } = this.props;	
		return (
				<Button variant="contained" className={classes.root} style={{marginRight: '3px', marginLeft: '3px'}} onClick={onClick}>
					{children} 
                </Button>
        );
	}
}


CustomIconButton = withStyles(buttonStyles)(CustomIconButton);