import React, {
    Component
} from 'react';
import {
    render
} from 'react-dom';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';

class LineChart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // To avoid unnecessary update keep all options in the state.
            chartOptions: {
                title: {
                    text: 'Title'
                },
                xAxis: {
                    categories: [],
                },
                series: [],
                plotOptions: {
                    series: {
                        point: {
                            events: {
                                mouseOver: this.setHoverData.bind(this)
                            }
                        }
                    }
                }
            },
            hoverData: null
        };
    }

    componentWillMount = () => {
        this.setState({chartOptions: {
                title: {
                    text: this.props.title
                },
                xAxis: {
                    categories: this.props.xAxisCategories,
                },
                series: [{
                        type: 'line',
                        name: "% Connectivity",
                        data: this.props.data.series1
                    },{
                        type: 'spline',
                        name: "% Connectivity",
                        data: this.props.data.series2
                    }
                ],
                plotOptions: {
                    series: {
                        point: {
                            events: {
                                mouseOver: this.setHoverData.bind(this)
                            }
                        }
                    }
                }
            }
        });
    }

    setHoverData = (e) => {
        // The chart is not updated because `chartOptions` has not changed.
        this.setState({
            hoverData: e.target.category
        })
    }

    updateSeries = () => {
        // The chart is updated only with new options.
        this.setState({
            chartOptions: {
                series: [{
                        data: [0, 0, 0,0,0]
                    },
                    // {
                    //     data: [0, 0, 0]
                    // },
                    // {
                    //     data: [0, 0, 0]
                    // }
                ]
            }
        });
    }

    render() {
        const {
            chartOptions,
            hoverData
        } = this.state;

        return ( <div >
                    <HighchartsReact highcharts = {Highcharts} options = {chartOptions}/> 
                    {/* <h3 > Hovering over {hoverData} </h3>  */}
                    {/* <button onClick = {
                        this.updateSeries.bind(this)
                    } > Update Series </button>  */}
                </div >
        )
    }
}

export default LineChart