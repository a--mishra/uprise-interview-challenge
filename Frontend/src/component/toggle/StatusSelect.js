import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { callbackify } from 'util';

// const PurpleSwitch = withStyles({
//   switchBase: {
//     color: '#fff',
//     '&$checked': {
//       color: '#fff',
//     },
//     '&$checked + $track': {
//       backgroundColor: '#3DB54A',
//       opacity: 1,
//     },
//   },
//   root: {
//     height:36
//   },
//   checked: {},
//   track: {},
// })(Switch);

const CustomSwitch = withStyles(theme => ({
  root: {
    width: 26,
    height: 14,
    padding: 0,
    marginLeft:15,
  },
  switchBase: {
    padding: 1,
    top:1,
    '&$checked': {
      transform: 'translateX(13px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: '#3DB54A',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#52d869',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 10,
    height: 10,  
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: `#999999`,
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});



export default function StatusSelect(props) {
  const [state, setState] = React.useState({
    checkedA: true,
  });

  const handleChange = (event,value) => {
    props.callback(value, props.id)
  };

  return (
      <FormControlLabel   className={CustomSwitch.root}
        control={
          <CustomSwitch
            checked={props.checkedValue}
            onChange={(event,value)=>handleChange(event,value)}
            value={props.checkedValue}
          />
        }
      />
  );
}
