import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';

import AddIcon from '@material-ui/icons/Add';
import {CustomIconButton} from '../../../component/widget/IconButton'

import { renderSelect, renderTextField, renderMUISelect} from '../../../component/form-elements/FormElements'

import { Grid, RadioGroup, FormControl, FormLabel, FormControlLabel, Radio, MenuItem } from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux'
import { initialize } from 'redux-form';

const styles = theme => ({
    textField : {
        width: '100%', 
    },
    dropDown: {
        width: '100%',
    },
    label: {
        display: "block",
        content: "",
        position: "absolute",
        top: "60px",
        opacity: "0",
        transition: ".2s opacity ease-out, .2s color ease-out",
        fontSize: '0.714rem',
        fontWeight: '500',
        color: '#999'
    },
    inputField: {
        marginTop: "10px",
        marginBottom: "4px",
    },
    root: {
        fontSize: "0.714rem",
        fontWeight: "500",
        color: "#999"
    }
});



const validate = values => {
  const errors = {}
  const requiredFields = ['ivrName', 'ivrType'];
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'Please fill out this field'
    }
  })
  return errors
}


class AddIVR extends Component {

  constructor(props){
    super(props);
    this.state = {
      open: false,
      ivrName: '',
      ivrType: '',
      ivrDescription:'',
      json_data:[],
      copyFromIVRTemplate:'',
      crossIcon:<i className='mdi mdi-close'></i>,
      isLoaded: false,
      disableSubmit: true
    }
  }

  componentWillMount() {
      // do nothing;
      let data = {};
      data.json_data = this.props.json_data ? this.props.json_data : [];
      this.props.dispatch(initialize('addIVRForm', this.props.row));
  }

  handleSubmit = (event) => {
    let settingsObject = {};
    settingsObject.ivrName = this.state.ivrName;
    settingsObject.ivrType = this.state.ivrType;
    settingsObject.json_data = this.state.json_data;
    settingsObject.ivrDescription = this.state.ivrDescription;
    settingsObject.copyFromIVRTemplate = this.state.copyFromIVRTemplate;
    this.props.callback(settingsObject);
    this.handleClose();
  }

  handleClickOpen = () => {
      this.setState({ open: true });
      this.setState({ivrName: '',
      ivrType: '',
      ivrDescription:'',
      copyFromIVRTemplate:''});
      this.props.reset()
    };
    
  handleClose = () => {
      this.setState({ open: false });
    };
  
  handleChange = (event, id) => {
    this.setState({[id] : event.target.value});
  }




  render() {
    const {classes} = this.props
    const { handleSubmit, pristine, reset, submitting, invalid } = this.props

    // window.log(this.props, 'props in AddIVR')
    var allOptions = {};

    let allIVRTypes = this.props.allIVRTypes;
    if (allIVRTypes !== undefined) {
      let options = []
      if(allIVRTypes.length == 1){
        // do nothing
      } else {
        options.push(<option key="s" value=""></option>);
      }
      
      Object.keys(allIVRTypes).forEach( (key,index) => {
                  options.push(<option
                    value={allIVRTypes[key].id}
                    key={index} 
                    >
                    {`${allIVRTypes[key].name}`}
                  </option>)
            });
      allOptions['allIVRTypes'] = options;
    }


    let ivrTemplates = this.props.ivrTemplates;
    if (ivrTemplates !== undefined) {
      let options = []
      if(ivrTemplates.length == 1){
        // do nothing
      } else {
        options.push(<option key="s" value=""></option>);
      }

      Object.keys(ivrTemplates).forEach( (key,index) => {
                  options.push(<option
                    value={ivrTemplates[key].id}
                    key={index} 
                    >
                    {`${ivrTemplates[key].name}`}
                  </option>)
            });
      allOptions['copyFromIVRTemplate'] = options;
    }
    


    return (
      <React.Fragment>
        {this.props.icon !== undefined && this.props.icon != null ?
          <div onClick={() => {this.handleClickOpen()}}>{this.props.icon}</div>
        :
          <CustomIconButton variant="contained" onClick={() => {this.handleClickOpen()} }><AddIcon fontSize="small"/></CustomIconButton>
        }<Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          fullWidth
          scroll='paper'
          style={{maxWidth: 500, minWidth: 500,
          position: "absolute",
          top: 'calc(max(20px, 40% - 400px))',
          left: 'calc(50% - 250px)'
        }}
        >
          <form onSubmit={handleSubmit}>

          <DialogTitle disableTypography className="modal-header clearfix" id="alert-dialog-title"><h3>Add IVR</h3>
              {/* <IconButton aria-label="Close" className="closeButton right" onClick={this.handleClose}>{this.state.crossIcon}</IconButton> */}
              <CustomIconButton aria-label="Close" className="closeButton right" onClick={this.handleClose}>{this.state.crossIcon}</CustomIconButton>
              
            </DialogTitle>
          <DialogContent className="modal-content">

            <Grid container>

                <Grid item sm={6}>
                    <Field component={renderTextField} name={`ivrName`} label="Name *" type="text" onChange={(e)=> this.handleChange(e, 'ivrName')}>
                    </Field>
                </Grid>
          


                <Grid item sm={6}>
                    <Field component={renderMUISelect} name={`ivrType`} label="Type *" type="text" onChange={(e)=> this.handleChange(e, 'ivrType')}>
                      {allOptions['allIVRTypes']}
                    </Field>
                </Grid>

                {this.props.json_data ? <></>: <Grid item sm={6}>
                    <Field component={renderMUISelect} name={`copyFromIVRTemplate`} label="Copy From IVR Template" type="text" onChange={(e)=> this.handleChange(e, 'copyFromIVRTemplate')}>
                      {allOptions['copyFromIVRTemplate']}
                    </Field>
                </Grid>}

                <Grid item sm={12}>
                    <Field component={renderTextField} name={`ivrDescription`} label="Description" type="text" onChange={(e)=> this.handleChange(e, 'ivrDescription')} multiline id="outlined-multiline-static" rows={4} variant="outlined" margin="normal" fullwidth size='Normal' InputLabelProps={{}} style={{width:'100%'}} >
                    </Field>
                </Grid>

                <Grid item sm={12} style={{display:"none"}}>
                    <Field component={renderTextField} name={`json_data`} label="Description" type="text" onChange={(e)=> this.handleChange(e, 'json_data')} multiline id="outlined-multiline-static" rows={4} variant="outlined" margin="normal" fullwidth size='Normal' InputLabelProps={{}} style={{width:'100%'}} value={ this.props.json_data ? this.props.json_data : []} hidden >
                    </Field>
                </Grid>

            </Grid>            

          </DialogContent>
          
          <DialogActions className="modal-footer clearfix right-align align-right">
            <Button className="btn btn-secondary button-light"  disabled={pristine || submitting} onClick={reset}>Clear</Button>
            <Button onClick={()=>{this.handleSubmit()}} className="btn btn-primary button-dark" autoFocus  disabled={invalid || pristine || submitting}>Apply</Button>
          </DialogActions>
          </form>
        </Dialog>

    </React.Fragment>
    );
  }
}


AddIVR = reduxForm({
  form: 'addIVRForm',
  validate,
  destroyOnUnmount: true,
  forceUnregisterOnUnmount: true
})(AddIVR)


AddIVR = connect(
  state => ({
    initialValues: state.form.addIVRForm !== undefined && (state.form.addIVRForm.values !== undefined ? state.form.addIVRForm.values : {})
  })
)(AddIVR)


const mapStateToProps = (state) => ({
  allIVRTypes : state.dataReducer && state.dataReducer.allIVRTypes ? state.dataReducer.allIVRTypes : [{id:'Inbound', name:'Inbound'}, {id:'MissedCall', name:'MissedCall'}],
  ivrTemplates : state.dataReducer && state.dataReducer.ivrTemplates ? state.dataReducer.ivrTemplates : [],
  templateSelectionInAddIVR : state.uiReducer && state.uiReducer.uiControlSettings && state.uiReducer.uiControlSettings.templateSelectionInAddIVR ? state.uiReducer.uiControlSettings.templateSelectionInAddIVR : true,
})

const mapToDispatchProps = (dispatch, ownProps) => ({
  createFormValues: () => {
    //dispatch(createFormValues())
  }
})


export default connect(mapStateToProps, mapToDispatchProps)(withStyles(styles)(AddIVR));