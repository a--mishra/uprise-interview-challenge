const dataReducer = (state = {}, action ) => {
    switch(action.type){

        case 'PROFILE_PLAYLISTS':
            return {
                ...state,
                profilePlaylists : action.data
            }

        case 'PROFILE':
            return {
                ...state,
                profile : action.data
            }        

        default : 
            return state;

    }
}

export default dataReducer