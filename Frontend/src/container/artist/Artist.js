import React, { Component } from 'react';
import {connect} from 'react-redux'

import { withStyles } from '@material-ui/core/styles';
import { Grid, Checkbox } from '@material-ui/core';
import SelectList from '../../component/select/SelectList'
import CustomizedBreadcrumbs from '../../component/navigation/CustomizedBreadcrumbs'
import AppInfoPopUp from '../../component/popover/AppInfoPopUp'
import {CustomIconButton} from '../../component/widget/IconButton'
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';

import AddIVR from './actionButtons/AddIVR';


import TuneIcon from '@material-ui/icons/Tune';
import SettingsInputComponentIcon from '@material-ui/icons/SettingsInputComponent';
import SettingsIcon from '@material-ui/icons/Settings';
import VisibilityIcon from '@material-ui/icons/Visibility';
import AddIcon from '@material-ui/icons/Add';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import TableChartIcon from '@material-ui/icons/TableChart';
import SettingsInputAntennaIcon from '@material-ui/icons/SettingsInputAntenna';

import 'status-indicator/styles.css' 

import { css } from '@emotion/core';

import {
    selectedIVR,
    createNewIVR,
    selectedView
} from '../../middlewares/Middleware'


const override = css`
    position: absolute;
    top: 36px;
    left: 50%;
    /* margin-right: -50%; */
    /* transform: translate(-50%, -50%)`;

const cssStyleForBreadCrum = {
    "position": "absolute",
    "top": "27px",
    "right": "16px"
}

const cssStyleForAddIVR = {
    "position": "relative",
    "top": "27px",
    "left": "16px"
}

const cssStyleForLiveStatus = {
    "position": "relative",
    "top": "27px",
    "left": "32px"
}

const cssStyleForAppInfo = {
    "position": "sticky",
    "bottom": "16px",
    "left": "0px"
}

const ivrSelectionContainer = {
    "display":"flex"
}

class TopBar extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            isLoaded: false,
            showPreview: false
        };
    }
    
    componentDidMount() {

    }

    refreshTableState = () => {

    }

    // parseTableData = (data) => {
    //     let selectedIVRId = this.props.selectedIVR.id;
    //     let tableData = [];

    //     Object.keys(data).forEach( (key, index) => {
    //         let currentData = data[key];
    //         let shouldInclude = true;

    //         if(shouldInclude){
    //             tableData.push(currentData);
    //         }

    //     })
    //     return tableData;
    // }


    onIVRSelected = (data) => {
        // do nothing;
        this.props.updateSelectedIVR(data);

    }

    showPreview = () => {
        this.setState({showPreview:true});
    }

    showDesigner = () => {
        this.setState({showPreview:false});
    }

    createNewIVR = (data) => {
        this.props.createNewIVR(data);
    }


    render() {

        let ivrOptionsList = [];
        let ivrList = this.props.ivrList == undefined ? [] : this.props.ivrList;

        Object.keys(ivrList).forEach( (keyName, keyIndex) => {
            let currentObject = ivrList[keyIndex];

            let tempObject = {};
            tempObject.id = currentObject.id;

            if(currentObject.enabled  == true)
                tempObject.title = `${window.String.fromCharCode(9733)} ${currentObject.name}`;
            else {
                tempObject.title = `${currentObject.name}`;
            }

            // tempObject.icon = <status-indicator positive pulse style={{marginRight:'5px'}}></status-indicator>;
            tempObject.category = currentObject.type;
            
            ivrOptionsList.push(tempObject);
            
        })
        
        let navigation = [
            {
                label:"Preview Mode",
                id:"Preview",
                icon:<AccountTreeIcon fontSize="small" />,
                callback: (callbackData)=>{
                    console.log(callbackData);
                    // alert(callbackData);
                    this.props.selectedView(callbackData)
                },
            },
            {
                label:"Edit Mode",
                id:"Designer",
                icon:<TableChartIcon fontSize="small" />,
                callback: (callbackData)=>{
                    console.log(callbackData);
                    // alert(callbackData)
                    this.props.selectedView(callbackData)
                },
            }
        ]

        // let addIVRAction = [
        //     {
        //         label:"Add IVR",
        //         id:"addIVR",
        //         icon:<TuneIcon fontSize="small" />,
        //         callback: (callbackData)=>{
        //             console.log(callbackData);
        //             alert(callbackData)
        //         },
        //     }
        // ]

        if(this.props.selectedIVR && this.props.selectedIVR.id !== undefined && this.props.selectedIVR.id != null ){
            // return Designer or Preview//
        } else {
            return (
                <React.Fragment>    
                    <Grid container className="page-layout-spacing" >
                        <Grid xs={12} sm={6} md={6} lg={6} >
                            <div style={ivrSelectionContainer}>
                                <SelectList optionsList={ivrOptionsList} callback={(ivrId)=>this.onIVRSelected(ivrId)} label={'Select IVR *'} />
                                <div style={cssStyleForAddIVR}>
                                    {/* <CustomIconButton variant="contained" onClick={() => {this.createNewIVR()}}><AddIcon fontSize="small" /></CustomIconButton> */}
                                    <AddIVR callback={(data) => {this.createNewIVR(data)}} ></AddIVR>
                                </div>
                                {/* <div style={cssStyleForLiveStatus}>
                                    <Chip
                                        variant="outlined"
                                        size="small"
                                        color="default"
                                        // avatar={<Avatar></Avatar>}
                                        // avatar={<Avatar><SettingsInputAntennaIcon fontSize="small" /></Avatar>}
                                        avatar={<SettingsInputAntennaIcon fontSize="small" />}
                                        // avatar={<CustomIconButton variant="contained" onClick={() => {alert('clicked icon')}}><SettingsInputAntennaIcon fontSize="small" /></CustomIconButton>}
                                        label="Live"
                                        onClick={()=>{alert('clicked')}}
                                    />
                                </div> */}
                            </div>
                            
                        </Grid>

                        <Grid xs={12} sm={6} md={6} lg={6}>
                            <div style={cssStyleForBreadCrum}>
                                <CustomizedBreadcrumbs tabs={navigation}/>
                            </div>
                        </Grid>

                    </Grid>
                </React.Fragment>
            );
        }


    }
}


const mapStateToProps = (state) => ({
    selectedIVR : state.uiReducer == undefined ? {} : state.uiReducer.selectedIVR == undefined ? {} : state.uiReducer.selectedIVR,
    ivrList : state.dataReducer == undefined ? [] : state.dataReducer.ivrList == undefined ? [] : state.dataReducer.ivrList,
})

const mapToDispatchProps = (dispatch, ownProps) => ({
    updateSelectedIVR: (ivrId) => {
        dispatch(selectedIVR(ivrId))
    },
    createNewIVR: (data) => {
        dispatch(createNewIVR(data));
    },
    selectedView: (data) => {
        dispatch(selectedView(data))
    }
})



export default connect(mapStateToProps, mapToDispatchProps)(TopBar);

