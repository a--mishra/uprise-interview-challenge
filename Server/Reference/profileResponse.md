{
  "data": {
    "user": {
      "display_name": "ChilledCow",
      "id": "chilledcow",
      "href": "https://api.spotify.com/v1/users/chilledcow",
      "images": [
        {
          "url": "https://i.scdn.co/image/ab6775700000ee85cbad6f248c4cc48a076d39a7",
          "width": null,
          "height": null
        }
      ],
      "playlists": [
        {
          "name": "lofi hip hop music - beats to relax/study to",
          "uri": "spotify:playlist:0vvXsWCC9xrXsKd4FyS8kM",
          "href": "https://api.spotify.com/v1/playlists/0vvXsWCC9xrXsKd4FyS8kM",
          "id": "0vvXsWCC9xrXsKd4FyS8kM",
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67706c0000bebb163aeea48afe86ed0c55bfcd",
              "width": null,
              "height": null
            }
          ],
          "description": "A daily selection of chill beats - perfect to help you relax &amp; study 📚",
          "tracks": [
            {
              "track": {
                "name": "Snowman",
                "id": "5ehVOwEZ1Q7Ckkdtq0dY1W",
                "uri": "spotify:track:5ehVOwEZ1Q7Ckkdtq0dY1W",
                "preview_url": "https://p.scdn.co/mp3-preview/b4762770e66b668fc303bee8c6155721d563df10?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "WYS",
                    "id": "2CiO7xWdwPMDlVwlt9qa1f"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Painting",
                "id": "2dXLYXOfQK5kURrG54kuE6",
                "uri": "spotify:track:2dXLYXOfQK5kURrG54kuE6",
                "preview_url": "https://p.scdn.co/mp3-preview/44f9c2d53c17f98fadf03d41fd9aea4976ccf3d3?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Lonely",
                "id": "1UkrOlQkhNI5a7i1R9hAu2",
                "uri": "spotify:track:1UkrOlQkhNI5a7i1R9hAu2",
                "preview_url": "https://p.scdn.co/mp3-preview/bb361d580287deec262854ba88f4f6664760f26f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Spencer Hunt",
                    "id": "4btBTQ1pWqpnDPY4BWMh1S"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Tea House",
                "id": "3OzqVfE4tMkBKxm6ecJD82",
                "uri": "spotify:track:3OzqVfE4tMkBKxm6ecJD82",
                "preview_url": "https://p.scdn.co/mp3-preview/a429b6ef77f1047587565981af926d9df53df59f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mondo Loops",
                    "id": "1XFN3VcuKr4tsTtQlRiTgK"
                  },
                  {
                    "name": "Kanisan",
                    "id": "0Q6S7QIOyuvDYzbhpvM5FO"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Nostalgia",
                "id": "5yQGijJH2I8wJXBJFA4hWE",
                "uri": "spotify:track:5yQGijJH2I8wJXBJFA4hWE",
                "preview_url": "https://p.scdn.co/mp3-preview/033001f0192b8c8edba66b31e69fb794829e3403?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Glimlip",
                    "id": "5wEF5my54dE5vMMmSUz2q3"
                  },
                  {
                    "name": "Sleepermane",
                    "id": "4gGsx7blPpBj7gKGmDBEfI"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Hushed Tones",
                "id": "1UMTVaGYOkLShJxtekqJbk",
                "uri": "spotify:track:1UMTVaGYOkLShJxtekqJbk",
                "preview_url": "https://p.scdn.co/mp3-preview/7b8daffc231577b8b29e232c7db79fcf1e31e6c0?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Oatmello",
                    "id": "0YAkOkbeAPiS35qyouiM4O"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "I Am Not Lost",
                "id": "0HCOG1BRF5TkwQtwBWiDvn",
                "uri": "spotify:track:0HCOG1BRF5TkwQtwBWiDvn",
                "preview_url": "https://p.scdn.co/mp3-preview/4746e45347c83d243259565fd0a0d57f4b7e90ed?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Nothingtosay",
                    "id": "53xwq54syWZgXoordzYUnA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Moonlight",
                "id": "2ZNhC4mJMn64VHkwuZkoQx",
                "uri": "spotify:track:2ZNhC4mJMn64VHkwuZkoQx",
                "preview_url": "https://p.scdn.co/mp3-preview/dad161de848dd029b4d4e8c5cf33990e6d5bb3b2?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Laffey",
                    "id": "7LWdcPFBFcRaamGjIJbPV7"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "I’ll Follow You",
                "id": "2gBeCFSAZ1qOufI51OUngK",
                "uri": "spotify:track:2gBeCFSAZ1qOufI51OUngK",
                "preview_url": "https://p.scdn.co/mp3-preview/d55be4398ac792eaa059158f9ebb881222e46ef7?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Nothingtosay",
                    "id": "53xwq54syWZgXoordzYUnA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Sashimi",
                "id": "1ER5CxMsKicKfSoVJyRi79",
                "uri": "spotify:track:1ER5CxMsKicKfSoVJyRi79",
                "preview_url": "https://p.scdn.co/mp3-preview/d4cdf5c01eefc9303c43d56b4671e2f5d9fa1809?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Dontcry",
                    "id": "3vzJueN7TkCtYpz1myVmDU"
                  },
                  {
                    "name": "Glimlip",
                    "id": "5wEF5my54dE5vMMmSUz2q3"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Microscopic",
                "id": "1XzsBm1dkNZNZFO0aUe5xH",
                "uri": "spotify:track:1XzsBm1dkNZNZFO0aUe5xH",
                "preview_url": "https://p.scdn.co/mp3-preview/da96872de208a78870ffdc9c187a14652facde0f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Pale Moon",
                "id": "7bxagI2dQL5NzM2vC2bPqc",
                "uri": "spotify:track:7bxagI2dQL5NzM2vC2bPqc",
                "preview_url": "https://p.scdn.co/mp3-preview/b5162e45ed76780de506c16527b16425f0990a6a?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Dr. Dundiff",
                    "id": "6T2NShr7SAArhtegdIpHHN"
                  },
                  {
                    "name": "Allem Iversom",
                    "id": "6RaUtVLO8R5TsVdJIxSrq1"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Rain on Sunday",
                "id": "27Y8JVUuqJcOpdNkXANrbl",
                "uri": "spotify:track:27Y8JVUuqJcOpdNkXANrbl",
                "preview_url": "https://p.scdn.co/mp3-preview/8f5fe99ab9b750b59379da9fddf2cf78bc8fe684?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Tom Doolie",
                    "id": "4C7NcNb9V6lakzMGHQlm8i"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Affection",
                "id": "4iIEbA1H7SGwlVyJPiroEI",
                "uri": "spotify:track:4iIEbA1H7SGwlVyJPiroEI",
                "preview_url": "https://p.scdn.co/mp3-preview/a473c34aab4473b63cc97a548927230e707c83fb?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Casiio",
                    "id": "5zUSfxfP1NETZiaWt0Ui0a"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Kiptime",
                "id": "4LNGK2QGAfNsQK8aQZ16LE",
                "uri": "spotify:track:4LNGK2QGAfNsQK8aQZ16LE",
                "preview_url": "https://p.scdn.co/mp3-preview/08dd6aa1fe59f972b9981e856b2f8b725bf704e6?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "HM Surf",
                    "id": "6TeBxtluBMQixZcKkJ3ZrB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Caramellow",
                "id": "2GX2qeMZL8XtvpfQwuolGP",
                "uri": "spotify:track:2GX2qeMZL8XtvpfQwuolGP",
                "preview_url": "https://p.scdn.co/mp3-preview/61cc7524c0e6d2ad82555ac51eee16b24a9133f5?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Purrple Cat",
                    "id": "73aKnLT4O8G2pBEfdlQzrE"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Distant Lands",
                "id": "3YiRy3520aDMGTRcDrFbrZ",
                "uri": "spotify:track:3YiRy3520aDMGTRcDrFbrZ",
                "preview_url": "https://p.scdn.co/mp3-preview/c57b4e170224e7efa857bd89ff06363f5e66018d?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Sing",
                "id": "6pU0n36bzWdGfvik5tR3eA",
                "uri": "spotify:track:6pU0n36bzWdGfvik5tR3eA",
                "preview_url": "https://p.scdn.co/mp3-preview/368f489b1f20bf56a9cb99c55d33a89df6564c3f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Under Your Skin",
                "id": "0fv5xuEjIr16jJ6z2GtpnC",
                "uri": "spotify:track:0fv5xuEjIr16jJ6z2GtpnC",
                "preview_url": "https://p.scdn.co/mp3-preview/ba3d7906e33a5b0cd1bed997ee228771f997cba1?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "BluntOne",
                    "id": "225l1KEprObX8xgl8xo2Gc"
                  },
                  {
                    "name": "Baen Mow",
                    "id": "2mt3wR9B4tg9KXvICFYhqM"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Snowflakes",
                "id": "3JNdPrGF30DLwMXSFeTdff",
                "uri": "spotify:track:3JNdPrGF30DLwMXSFeTdff",
                "preview_url": "https://p.scdn.co/mp3-preview/7164048af1a4e731ac2f6bbb1a3c2a45a8639b02?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Pandrezz",
                    "id": "65ZGdYSRT3Rmv6P7DN4XCC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Before",
                "id": "6R21mLVsXFN5I3PpoOsRIf",
                "uri": "spotify:track:6R21mLVsXFN5I3PpoOsRIf",
                "preview_url": "https://p.scdn.co/mp3-preview/51e73fc51b42c9982354f63e4d08ee9dfee51af6?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Chiccote's Beats",
                    "id": "0ETiNCZavTPXNnEJBF1JBA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Obscurity",
                "id": "5a2zoElcTekbJM8ASl7skW",
                "uri": "spotify:track:5a2zoElcTekbJM8ASl7skW",
                "preview_url": "https://p.scdn.co/mp3-preview/79c5ad6abae268de0c38ad49208c82263a18bba4?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Chris Mazuera",
                    "id": "3Sb3oI3Xw7FcgYS262zXPE"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Heroes",
                "id": "3qHragQorghdC0TpIa6A0k",
                "uri": "spotify:track:3qHragQorghdC0TpIa6A0k",
                "preview_url": "https://p.scdn.co/mp3-preview/56b6dcc3b55c66de55397df3d61bd9985129ae87?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Cruisin'",
                "id": "0GI3utI1PXaCMiQTq1l3di",
                "uri": "spotify:track:0GI3utI1PXaCMiQTq1l3di",
                "preview_url": "https://p.scdn.co/mp3-preview/70dcad61475ba19ddfc3cb191b12f44ce27c97d7?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Elior",
                    "id": "0tquhVod10o2zf1ht2aVoz"
                  },
                  {
                    "name": "eaup",
                    "id": "5MvvhhTGyd2iGzaksZpLEt"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Sylvan",
                "id": "6J80IvbXCCLBppDLaMCNbZ",
                "uri": "spotify:track:6J80IvbXCCLBppDLaMCNbZ",
                "preview_url": "https://p.scdn.co/mp3-preview/c26b40b70b6a039895d74a943fe1b0d10fce7fa5?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Deserved Rest",
                "id": "539QeZzK7P9xtQfn8aXgBX",
                "uri": "spotify:track:539QeZzK7P9xtQfn8aXgBX",
                "preview_url": "https://p.scdn.co/mp3-preview/9c54ba1ae4595395d8b1d2212711744619b3e64b?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Pandrezz",
                    "id": "65ZGdYSRT3Rmv6P7DN4XCC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Everything's a Symptom",
                "id": "4Xf0CTS2WoDKqkUy8CPE5C",
                "uri": "spotify:track:4Xf0CTS2WoDKqkUy8CPE5C",
                "preview_url": "https://p.scdn.co/mp3-preview/ea10c366ff0b34f88cd6d624918abe76bc91e290?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "less.people",
                    "id": "0QmdasntOdQpEwRd40wyp3"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Remorse",
                "id": "3ES0RvUaPzfCymyGTVUIg6",
                "uri": "spotify:track:3ES0RvUaPzfCymyGTVUIg6",
                "preview_url": "https://p.scdn.co/mp3-preview/f16611a9611cde357eec1a20ddc9b352f9d7e303?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Thaehan",
                    "id": "2n0sjgdrfHYJT6B39cdvWW"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Empty Shelves",
                "id": "5CMKVuY2EY2XIM2yxfMlgj",
                "uri": "spotify:track:5CMKVuY2EY2XIM2yxfMlgj",
                "preview_url": "https://p.scdn.co/mp3-preview/1e96129f81983a28c6e16d79a6630a31ed4bbfd8?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Team Astro",
                    "id": "5BJiJign2sqMUCJhpStgNd"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Enough to Go Around",
                "id": "0g2cKbRSGDlhfbrVWOhvwG",
                "uri": "spotify:track:0g2cKbRSGDlhfbrVWOhvwG",
                "preview_url": "https://p.scdn.co/mp3-preview/77a5d89de561029cbab42b9e441d3eb6cedb8e6e?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "less.people",
                    "id": "0QmdasntOdQpEwRd40wyp3"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Day 12",
                "id": "51Dae1jpRALuKCnR9LofRZ",
                "uri": "spotify:track:51Dae1jpRALuKCnR9LofRZ",
                "preview_url": "https://p.scdn.co/mp3-preview/f1e4a50186314127732850d34c4e0ac762d49f4d?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Chris Mazuera",
                    "id": "3Sb3oI3Xw7FcgYS262zXPE"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Silent River",
                "id": "422HHhVoCh2n5hAU7l6JID",
                "uri": "spotify:track:422HHhVoCh2n5hAU7l6JID",
                "preview_url": "https://p.scdn.co/mp3-preview/4b9a8670d4982fd0093e99967154347a1ba1a33b?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mila Coolness",
                    "id": "3gdyXwWMfOPBIZrIDMg40u"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Campfire",
                "id": "68gNCjyHv5rUv3IVx9Oo0w",
                "uri": "spotify:track:68gNCjyHv5rUv3IVx9Oo0w",
                "preview_url": "https://p.scdn.co/mp3-preview/895b9737228f30a5393012f6ccf2def63ff65371?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Laffey",
                    "id": "7LWdcPFBFcRaamGjIJbPV7"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Saturday",
                "id": "3HDdRuvHaTWMxAXea7M8qc",
                "uri": "spotify:track:3HDdRuvHaTWMxAXea7M8qc",
                "preview_url": "https://p.scdn.co/mp3-preview/7d62f5d43f8637895203af8948f2d8944329cf15?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "softy",
                    "id": "0wcen0V8FgQu6xYupnZMbB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Away from Home",
                "id": "5gnMK58Kq7iZxWprebXz4u",
                "uri": "spotify:track:5gnMK58Kq7iZxWprebXz4u",
                "preview_url": "https://p.scdn.co/mp3-preview/51d1a01952d418d187c1aa20a1bf9751a00a0245?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Jhove",
                    "id": "1R9fj5Tiy9XMFp5ANzS7FA"
                  },
                  {
                    "name": "Bert",
                    "id": "3Zi91vuWAU6SUwcKDf8zDq"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Abandoned",
                "id": "1FnUwnhTy1hI9JWKRW5LE2",
                "uri": "spotify:track:1FnUwnhTy1hI9JWKRW5LE2",
                "preview_url": "https://p.scdn.co/mp3-preview/97a11e859e45c91e2a500d01093789d19f9ec4c0?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "DLJ",
                    "id": "3chQixmxhv9UmwQc8aBApA"
                  },
                  {
                    "name": "TABAL",
                    "id": "3xGCm4ewXGKJFwrrwummsJ"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Those Were the Days",
                "id": "3Y5NZQJa02QUxSHCYH4nf4",
                "uri": "spotify:track:3Y5NZQJa02QUxSHCYH4nf4",
                "preview_url": "https://p.scdn.co/mp3-preview/8390240a55e5c67c36d4ac84109c4477899c6e25?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "I Can’t Sleep",
                "id": "0jN2HkGxd3fCesdQCCUovV",
                "uri": "spotify:track:0jN2HkGxd3fCesdQCCUovV",
                "preview_url": "https://p.scdn.co/mp3-preview/ba176374eded7db6e90b8c2530c66d770c20e4b2?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Nothingtosay",
                    "id": "53xwq54syWZgXoordzYUnA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Far Away",
                "id": "0R1BXxdZMw2wnrm6OcXx6s",
                "uri": "spotify:track:0R1BXxdZMw2wnrm6OcXx6s",
                "preview_url": "https://p.scdn.co/mp3-preview/b5162fe18fb3805f6772bdb100b208679ce43cc5?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mila Coolness",
                    "id": "3gdyXwWMfOPBIZrIDMg40u"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Gloomy",
                "id": "1YWMRotYNbf1zL6bnSL6I0",
                "uri": "spotify:track:1YWMRotYNbf1zL6bnSL6I0",
                "preview_url": "https://p.scdn.co/mp3-preview/c9ee28e20c945afef1e2f8a374be246df93a67eb?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Laffey",
                    "id": "7LWdcPFBFcRaamGjIJbPV7"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Habits",
                "id": "0dbRKSq6nUKHrjKwFztnzZ",
                "uri": "spotify:track:0dbRKSq6nUKHrjKwFztnzZ",
                "preview_url": "https://p.scdn.co/mp3-preview/af61936460341a11ba21b4367dc18f3d4832565f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Allem Iversom",
                    "id": "6RaUtVLO8R5TsVdJIxSrq1"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Ibis",
                "id": "3gbS6OFQhtQkZaCSUZxydP",
                "uri": "spotify:track:3gbS6OFQhtQkZaCSUZxydP",
                "preview_url": "https://p.scdn.co/mp3-preview/c5ae30b03aaf5ed58ef2d6577bce18127efc2ad1?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mila Coolness",
                    "id": "3gdyXwWMfOPBIZrIDMg40u"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Pink Night Sky",
                "id": "0h9seLFY9jCf3m4kADyq1l",
                "uri": "spotify:track:0h9seLFY9jCf3m4kADyq1l",
                "preview_url": "https://p.scdn.co/mp3-preview/877526aa6777fc561d7ded91f36909f5cb4bb817?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Dr. Dundiff",
                    "id": "6T2NShr7SAArhtegdIpHHN"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Floating Away",
                "id": "4joQAEtYMSsRjfpt38qwwC",
                "uri": "spotify:track:4joQAEtYMSsRjfpt38qwwC",
                "preview_url": "https://p.scdn.co/mp3-preview/0a155ae1ba9e1ed0229c160ad19b6d149107292e?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Glimlip",
                    "id": "5wEF5my54dE5vMMmSUz2q3"
                  },
                  {
                    "name": "Yasper",
                    "id": "1axdL80XjVHdInGsJbURyt"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Sleepy Little One",
                "id": "26JfvzihGUTJs8vFEi4Nkn",
                "uri": "spotify:track:26JfvzihGUTJs8vFEi4Nkn",
                "preview_url": "https://p.scdn.co/mp3-preview/bfb489e55e2678a796b63d037d18ef6742681021?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Teakwood",
                "id": "1xW9riep92zpGZAW0HgmXJ",
                "uri": "spotify:track:1xW9riep92zpGZAW0HgmXJ",
                "preview_url": "https://p.scdn.co/mp3-preview/2472e428a69550db99c0417d5ea1da585a8c81c1?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Aso",
                    "id": "45Ui3GdcxzbdJhhTtZLXO8"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Twilight",
                "id": "2XVsNLaBJqjMoL79dHnNg7",
                "uri": "spotify:track:2XVsNLaBJqjMoL79dHnNg7",
                "preview_url": "https://p.scdn.co/mp3-preview/c4360d9e0d50b8a6d3579408d28c154cb8cd7410?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Further",
                "id": "0mlbXB8AukvaQxhatnWjOc",
                "uri": "spotify:track:0mlbXB8AukvaQxhatnWjOc",
                "preview_url": "https://p.scdn.co/mp3-preview/b500ef7db06d018b686f863f2409c9549129f39c?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "DLJ",
                    "id": "3chQixmxhv9UmwQc8aBApA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Nighttime",
                "id": "0qXw2PAI6WjC8cjG0Yc6Kw",
                "uri": "spotify:track:0qXw2PAI6WjC8cjG0Yc6Kw",
                "preview_url": "https://p.scdn.co/mp3-preview/7e2d6f707f635dac6ceb4fc49b37d84260a4a664?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Laffey",
                    "id": "7LWdcPFBFcRaamGjIJbPV7"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Streetlights",
                "id": "0j3sNE9xsyDhBq6vArgOPK",
                "uri": "spotify:track:0j3sNE9xsyDhBq6vArgOPK",
                "preview_url": "https://p.scdn.co/mp3-preview/4026d185e266536e487b21d761b3c4371f196e02?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Laffey",
                    "id": "7LWdcPFBFcRaamGjIJbPV7"
                  }
                ]
              }
            }
          ]
        },
        {
          "name": "lofi hip hop music - beats to sleep/chill to",
          "uri": "spotify:playlist:5FmmxErJczcrEwIFGIviYo",
          "href": "https://api.spotify.com/v1/playlists/5FmmxErJczcrEwIFGIviYo",
          "id": "5FmmxErJczcrEwIFGIviYo",
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67706c0000bebb82ebb6b692438d804e7b75b2",
              "width": null,
              "height": null
            }
          ],
          "description": "A daily selection of the smoothest lofi hip hop beats, perfect to help you chill or fall asleep 💤",
          "tracks": [
            {
              "track": {
                "name": "Sun",
                "id": "12p9iameBtppGCoMPlLVxz",
                "uri": "spotify:track:12p9iameBtppGCoMPlLVxz",
                "preview_url": "https://p.scdn.co/mp3-preview/32634d1721371f4ccbcc060ac429b03916e66e1c?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Abundance",
                "id": "6xs8EjmlSVTIDVUYj4REQW",
                "uri": "spotify:track:6xs8EjmlSVTIDVUYj4REQW",
                "preview_url": "https://p.scdn.co/mp3-preview/7d2c0c6fae6be1daddc260f10c8dbe42995decc3?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Chris Mazuera",
                    "id": "3Sb3oI3Xw7FcgYS262zXPE"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Becoming",
                "id": "2CStWGYWH8S11Hf9TYwYzK",
                "uri": "spotify:track:2CStWGYWH8S11Hf9TYwYzK",
                "preview_url": "https://p.scdn.co/mp3-preview/1056c043b34fa4d45e5ba24acfea8fcb22c0e5ca?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Flovry",
                    "id": "2pLu3Ut2C3RviYZ3xUanBs"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Mmmm",
                "id": "2cp3xfiYmcaQufk5ipRT4S",
                "uri": "spotify:track:2cp3xfiYmcaQufk5ipRT4S",
                "preview_url": "https://p.scdn.co/mp3-preview/8431e02fc791279d743f6073d74e55bb7d32e381?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "G Mills",
                    "id": "0djvqMepj2XkHfvWTqkH1N"
                  },
                  {
                    "name": "HM Surf",
                    "id": "6TeBxtluBMQixZcKkJ3ZrB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "C U in Class!",
                "id": "2mT9D5x506IrxZUCMgZmz5",
                "uri": "spotify:track:2mT9D5x506IrxZUCMgZmz5",
                "preview_url": "https://p.scdn.co/mp3-preview/273179276c9dc80be57ff0d6facc84448afd3225?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Flovry",
                    "id": "2pLu3Ut2C3RviYZ3xUanBs"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Still Dreaming",
                "id": "1JmUkmj6j8fV9Dr6A2CKy0",
                "uri": "spotify:track:1JmUkmj6j8fV9Dr6A2CKy0",
                "preview_url": "https://p.scdn.co/mp3-preview/d89454079b1d0a126b6990d5748f6331f5d48763?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "Pointy Features",
                    "id": "2juH2VRAmfRlPlu1oaGsuH"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Elsewhere",
                "id": "1D7xJOejClBN7QoklbbNPU",
                "uri": "spotify:track:1D7xJOejClBN7QoklbbNPU",
                "preview_url": "https://p.scdn.co/mp3-preview/6dbd12b372eb0e3c54628974af2348d6329d1d39?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "H.1",
                    "id": "3azKf6nXrUCI1RLZkX4Aj6"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Midnight Thoughts",
                "id": "6KpNNdEXCkYDhWKlBfNeSx",
                "uri": "spotify:track:6KpNNdEXCkYDhWKlBfNeSx",
                "preview_url": "https://p.scdn.co/mp3-preview/a86f60989535ed6da17429419d363158f3da3317?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Yasumu",
                    "id": "53rCVzFVlyntj7jEjnY2oM"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Love Letter",
                "id": "24z9ftGPfNkZoSmEOG8AWf",
                "uri": "spotify:track:24z9ftGPfNkZoSmEOG8AWf",
                "preview_url": "https://p.scdn.co/mp3-preview/3fba68f03d542f867a64a4b6cebb72168f91955e?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Hoogway",
                    "id": "1Mh9G47YfuaLdQs44voLrQ"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Before",
                "id": "6R21mLVsXFN5I3PpoOsRIf",
                "uri": "spotify:track:6R21mLVsXFN5I3PpoOsRIf",
                "preview_url": "https://p.scdn.co/mp3-preview/51e73fc51b42c9982354f63e4d08ee9dfee51af6?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Chiccote's Beats",
                    "id": "0ETiNCZavTPXNnEJBF1JBA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Lush",
                "id": "37RrS9j4SOybq4v0mOIOcd",
                "uri": "spotify:track:37RrS9j4SOybq4v0mOIOcd",
                "preview_url": "https://p.scdn.co/mp3-preview/5a02dba8dd9ebe14f818af421c3035642d49f524?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Casiio",
                    "id": "5zUSfxfP1NETZiaWt0Ui0a"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Gloom",
                "id": "5rBb2uP6oXtenyXSjNee6W",
                "uri": "spotify:track:5rBb2uP6oXtenyXSjNee6W",
                "preview_url": "https://p.scdn.co/mp3-preview/1ffdb98dbd0a5ce63a505c87d7a0556aad1b8638?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "mell-ø",
                    "id": "6bA2OonnJsG1tN9yClu2aC"
                  },
                  {
                    "name": "Ambulo",
                    "id": "6sPQwc6lix6K1Gv64v91Ml"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Station",
                "id": "4ouJ5sGG6EcLQ1gRUhwVG4",
                "uri": "spotify:track:4ouJ5sGG6EcLQ1gRUhwVG4",
                "preview_url": "https://p.scdn.co/mp3-preview/f55cb1a1a462e968c801fa6334cc8c362d92b849?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Habits",
                "id": "0dbRKSq6nUKHrjKwFztnzZ",
                "uri": "spotify:track:0dbRKSq6nUKHrjKwFztnzZ",
                "preview_url": "https://p.scdn.co/mp3-preview/af61936460341a11ba21b4367dc18f3d4832565f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Allem Iversom",
                    "id": "6RaUtVLO8R5TsVdJIxSrq1"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Before You Go",
                "id": "1PbE9iY55VhhdPeUHytI7S",
                "uri": "spotify:track:1PbE9iY55VhhdPeUHytI7S",
                "preview_url": "https://p.scdn.co/mp3-preview/474c4a12d1ad828d4e5c2ab983211c6f54cdc794?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Jhove",
                    "id": "1R9fj5Tiy9XMFp5ANzS7FA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Small Things",
                "id": "3sOHHdW5siv6AbFob2Bi8K",
                "uri": "spotify:track:3sOHHdW5siv6AbFob2Bi8K",
                "preview_url": "https://p.scdn.co/mp3-preview/ea0ef6a40f190ac7483705112b5172794ca02d36?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "softy",
                    "id": "0wcen0V8FgQu6xYupnZMbB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Ponds",
                "id": "7f0rJKftzMLz8Lul3cyXrD",
                "uri": "spotify:track:7f0rJKftzMLz8Lul3cyXrD",
                "preview_url": "https://p.scdn.co/mp3-preview/ce2caf02dceccc3d839cad1bc0a9b08300e2d895?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Elior",
                    "id": "0tquhVod10o2zf1ht2aVoz"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Teakwood",
                "id": "1xW9riep92zpGZAW0HgmXJ",
                "uri": "spotify:track:1xW9riep92zpGZAW0HgmXJ",
                "preview_url": "https://p.scdn.co/mp3-preview/2472e428a69550db99c0417d5ea1da585a8c81c1?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Aso",
                    "id": "45Ui3GdcxzbdJhhTtZLXO8"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Under Your Skin",
                "id": "0fv5xuEjIr16jJ6z2GtpnC",
                "uri": "spotify:track:0fv5xuEjIr16jJ6z2GtpnC",
                "preview_url": "https://p.scdn.co/mp3-preview/ba3d7906e33a5b0cd1bed997ee228771f997cba1?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "BluntOne",
                    "id": "225l1KEprObX8xgl8xo2Gc"
                  },
                  {
                    "name": "Baen Mow",
                    "id": "2mt3wR9B4tg9KXvICFYhqM"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Bodies",
                "id": "03nNNb5WxCmfoDB8flGdra",
                "uri": "spotify:track:03nNNb5WxCmfoDB8flGdra",
                "preview_url": "https://p.scdn.co/mp3-preview/94b039d2da44b3bebbf3d6eb8d08e0b8420962e6?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Sugar Coat",
                "id": "3FtNyYi6ejoytUZdT3WfNN",
                "uri": "spotify:track:3FtNyYi6ejoytUZdT3WfNN",
                "preview_url": "https://p.scdn.co/mp3-preview/7f56a8aedea2213f78f31f18e1b25050da1b08a0?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Purrple Cat",
                    "id": "73aKnLT4O8G2pBEfdlQzrE"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Thoughts of You",
                "id": "03mOTxo1RucacNpaWpv6gE",
                "uri": "spotify:track:03mOTxo1RucacNpaWpv6gE",
                "preview_url": "https://p.scdn.co/mp3-preview/8d54a8908da501f878f6654db8a681ee7db25cec?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Spencer Hunt",
                    "id": "4btBTQ1pWqpnDPY4BWMh1S"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Remember",
                "id": "3od77gHxwUxRNrCBvLaJkG",
                "uri": "spotify:track:3od77gHxwUxRNrCBvLaJkG",
                "preview_url": "https://p.scdn.co/mp3-preview/79e2bc19b187e95bcb53b4f4ad0f5772b1bc813f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Flovry",
                    "id": "2pLu3Ut2C3RviYZ3xUanBs"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Lonely",
                "id": "1UkrOlQkhNI5a7i1R9hAu2",
                "uri": "spotify:track:1UkrOlQkhNI5a7i1R9hAu2",
                "preview_url": "https://p.scdn.co/mp3-preview/bb361d580287deec262854ba88f4f6664760f26f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Spencer Hunt",
                    "id": "4btBTQ1pWqpnDPY4BWMh1S"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Pillows",
                "id": "5tiCooRjzp9HBYEznZ8YG5",
                "uri": "spotify:track:5tiCooRjzp9HBYEznZ8YG5",
                "preview_url": "https://p.scdn.co/mp3-preview/34d6be4fec9bd3a4c48bdedec5beb1d0c3832d3c?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Towerz",
                    "id": "1bbah9s09626gweOzzLbKG"
                  },
                  {
                    "name": "Spencer Hunt",
                    "id": "4btBTQ1pWqpnDPY4BWMh1S"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Aconitum",
                "id": "38lWgLPBoLVEwqk7XZkEPI",
                "uri": "spotify:track:38lWgLPBoLVEwqk7XZkEPI",
                "preview_url": "https://p.scdn.co/mp3-preview/00896c1729d848002784a708464859d0c5804735?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "StackOne",
                    "id": "7FePTbcF5rhNCiDQlunIHI"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Felt the Same",
                "id": "3lZowQpsIp4KrfS6WJi6OY",
                "uri": "spotify:track:3lZowQpsIp4KrfS6WJi6OY",
                "preview_url": "https://p.scdn.co/mp3-preview/61bd8a570593629f3f60f94947ff5245b2d58e06?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "softy",
                    "id": "0wcen0V8FgQu6xYupnZMbB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Breath",
                "id": "4nENd5X5SHvcjdszJHyRc2",
                "uri": "spotify:track:4nENd5X5SHvcjdszJHyRc2",
                "preview_url": "https://p.scdn.co/mp3-preview/35bb4334010a3aa7f164a39225a3c24267e6985d?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "H.1",
                    "id": "3azKf6nXrUCI1RLZkX4Aj6"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "If Only You Knew",
                "id": "7xmHPwtWRIleQIAM5vQURY",
                "uri": "spotify:track:7xmHPwtWRIleQIAM5vQURY",
                "preview_url": "https://p.scdn.co/mp3-preview/9d5b2d71e92e9a7c772027559c7bc54808cbad73?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Jhove",
                    "id": "1R9fj5Tiy9XMFp5ANzS7FA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Don't Let Go",
                "id": "7mtdUu2Yq1ttUzuL4GZbu3",
                "uri": "spotify:track:7mtdUu2Yq1ttUzuL4GZbu3",
                "preview_url": "https://p.scdn.co/mp3-preview/40ea641c360f7c6d9a6a5976b008144aed7c126a?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Blue Wednesday",
                    "id": "7185Q95lPFld0aoPqO6e0U"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Lunar Drive",
                "id": "4kOAvJlgkqzXtzcTI7ZBO3",
                "uri": "spotify:track:4kOAvJlgkqzXtzcTI7ZBO3",
                "preview_url": "https://p.scdn.co/mp3-preview/b2655bc5cb51d4c82e7a62c2ca112e8ab502452b?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mondo Loops",
                    "id": "1XFN3VcuKr4tsTtQlRiTgK"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Recess",
                "id": "3LiLe6IClT2z8WTr7G1LER",
                "uri": "spotify:track:3LiLe6IClT2z8WTr7G1LER",
                "preview_url": "https://p.scdn.co/mp3-preview/bf9eed0b6f9f2db65a62d8177d9a0f84c98cf15b?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Flovry",
                    "id": "2pLu3Ut2C3RviYZ3xUanBs"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Painting",
                "id": "2dXLYXOfQK5kURrG54kuE6",
                "uri": "spotify:track:2dXLYXOfQK5kURrG54kuE6",
                "preview_url": "https://p.scdn.co/mp3-preview/44f9c2d53c17f98fadf03d41fd9aea4976ccf3d3?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Stray",
                "id": "4XaeYKGQIEPmlz2oYACbcQ",
                "uri": "spotify:track:4XaeYKGQIEPmlz2oYACbcQ",
                "preview_url": "https://p.scdn.co/mp3-preview/3e41ced1ca4b00c02335bfe9d55e8e87a49a22f6?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Casiio",
                    "id": "5zUSfxfP1NETZiaWt0Ui0a"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Through the Clouds",
                "id": "65rgrr0kAPjOkeWA2bDoUQ",
                "uri": "spotify:track:65rgrr0kAPjOkeWA2bDoUQ",
                "preview_url": "https://p.scdn.co/mp3-preview/2341e921a96b04d87c02f24db821c684508ace33?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Autumn Morning",
                "id": "1zqkbok4NBWh5UHbQi6jmw",
                "uri": "spotify:track:1zqkbok4NBWh5UHbQi6jmw",
                "preview_url": "https://p.scdn.co/mp3-preview/a3ba5ae8a0a754c424f2add1320315714e631553?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "softy",
                    "id": "0wcen0V8FgQu6xYupnZMbB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Winter Shells",
                "id": "76pZiU5vU5OrRY0PPleJj1",
                "uri": "spotify:track:76pZiU5vU5OrRY0PPleJj1",
                "preview_url": "https://p.scdn.co/mp3-preview/efdb53824767ede00a7967de8faa903150a8a531?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mondo Loops",
                    "id": "1XFN3VcuKr4tsTtQlRiTgK"
                  },
                  {
                    "name": "Kanisan",
                    "id": "0Q6S7QIOyuvDYzbhpvM5FO"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "It All",
                "id": "22pZ6YWLkmkk4o1o20oNyV",
                "uri": "spotify:track:22pZ6YWLkmkk4o1o20oNyV",
                "preview_url": "https://p.scdn.co/mp3-preview/f4de872e4a17a576766906eb014bac4dba2c5099?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Jhove",
                    "id": "1R9fj5Tiy9XMFp5ANzS7FA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Fly",
                "id": "16GPk2JZmPImqXiJyXqaMc",
                "uri": "spotify:track:16GPk2JZmPImqXiJyXqaMc",
                "preview_url": "https://p.scdn.co/mp3-preview/477dd595a05ecb202027f6bb43b92ec5cf8fb4cc?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Brenky",
                    "id": "3U6rNiL0weudbwLofMicVq"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Pretend",
                "id": "4bDMa4P7uIaHMsvoiDNFYM",
                "uri": "spotify:track:4bDMa4P7uIaHMsvoiDNFYM",
                "preview_url": "https://p.scdn.co/mp3-preview/81069ee230a881deb72138b944ffb535f006a0b5?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Retro Jungle",
                    "id": "3gVC68xFogCXS5AeSBnLSP"
                  },
                  {
                    "name": "DLJ",
                    "id": "3chQixmxhv9UmwQc8aBApA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Inspect",
                "id": "3lvs7wY2cLxckUfJ0JojEd",
                "uri": "spotify:track:3lvs7wY2cLxckUfJ0JojEd",
                "preview_url": "https://p.scdn.co/mp3-preview/b4b6eed6f9563d82f5ec10c1984cf62c0e6d5a0f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Nothingtosay",
                    "id": "53xwq54syWZgXoordzYUnA"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "goodnight",
                "id": "0mYboMUgKxiLXnfH8mVTZC",
                "uri": "spotify:track:0mYboMUgKxiLXnfH8mVTZC",
                "preview_url": null,
                "artists": [
                  {
                    "name": "chief.",
                    "id": "0HCAzT0cSCpiNje7AcAQaD"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Carousel",
                "id": "7H8kLPsJuX21kJkorKu1IX",
                "uri": "spotify:track:7H8kLPsJuX21kJkorKu1IX",
                "preview_url": "https://p.scdn.co/mp3-preview/14d9d6edc06e2f78e99cd92a576fbd7dc2599eb6?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Hoogway",
                    "id": "1Mh9G47YfuaLdQs44voLrQ"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Just Close Your Eyes",
                "id": "4fZNHT9xCMtpWbLNrMXH44",
                "uri": "spotify:track:4fZNHT9xCMtpWbLNrMXH44",
                "preview_url": "https://p.scdn.co/mp3-preview/576ae1e455f91aa430ba0428a97bde1a76580e35?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "Lucid Green",
                    "id": "587BLbZ68izUrdPtYAFYfs"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Nightlight",
                "id": "5IxvyqAw1Kg7eNWORmdR3R",
                "uri": "spotify:track:5IxvyqAw1Kg7eNWORmdR3R",
                "preview_url": "https://p.scdn.co/mp3-preview/c45b2b5052df251e680ca602eb71025afd7bcb11?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "Nolfo",
                    "id": "1C167zA6xYX7sHxJYsAm3e"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Pillow",
                "id": "1sixIJgnbZZpr92ZSd6CHy",
                "uri": "spotify:track:1sixIJgnbZZpr92ZSd6CHy",
                "preview_url": "https://p.scdn.co/mp3-preview/e0b29f98d4b092e777a5ef06bf1d52ec7a200e8d?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Miscél",
                    "id": "5P8zM1rnE2owF1M5HzAuM5"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Ghost in My Mind",
                "id": "7B3ddhlG9jP8PeybwVyCq5",
                "uri": "spotify:track:7B3ddhlG9jP8PeybwVyCq5",
                "preview_url": "https://p.scdn.co/mp3-preview/68a393553ba20432d7bb567702d543e51a1d528a?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "j'san",
                    "id": "5iMUho98faEp2w6j5p44PH"
                  },
                  {
                    "name": "Epektase",
                    "id": "31jYTsfmnHqcK7ahdqlqmo"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Breeze",
                "id": "0rwnpmJr7XP4CfQxE5vYCy",
                "uri": "spotify:track:0rwnpmJr7XP4CfQxE5vYCy",
                "preview_url": "https://p.scdn.co/mp3-preview/c751eab3c61e1c2b8e1eec1b033337ddf23964b1?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mvdb",
                    "id": "0hleYpwrJSPEh2pCRTU0AY"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Ease Out",
                "id": "5cyZufWEEbEpZgpqqDCA5x",
                "uri": "spotify:track:5cyZufWEEbEpZgpqqDCA5x",
                "preview_url": "https://p.scdn.co/mp3-preview/88844bada24d48c84159552d14c2558ac4636950?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "softy",
                    "id": "0wcen0V8FgQu6xYupnZMbB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Off",
                "id": "6O5mlTWvXobRViLMhB3lkV",
                "uri": "spotify:track:6O5mlTWvXobRViLMhB3lkV",
                "preview_url": "https://p.scdn.co/mp3-preview/b80cd3d9d6ed220e8920c9bb89ed1ff53cf42203?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            }
          ]
        },
        {
          "name": "ChilledCow's favorites",
          "uri": "spotify:playlist:31FWVQBp3WQydWLNhO0ACi",
          "href": "https://api.spotify.com/v1/playlists/31FWVQBp3WQydWLNhO0ACi",
          "id": "31FWVQBp3WQydWLNhO0ACi",
          "images": [
            {
              "url": "https://i.scdn.co/image/ab67706c0000bebb3b73bcbc744c142e1732e158",
              "width": null,
              "height": null
            }
          ],
          "description": "",
          "tracks": [
            {
              "track": {
                "name": "Keyframe",
                "id": "7Fmc8OIzeIjWFcF5VxuT84",
                "uri": "spotify:track:7Fmc8OIzeIjWFcF5VxuT84",
                "preview_url": "https://p.scdn.co/mp3-preview/0d23d24139676411d18f1a5d0d3d76358688b3bb?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "G Mills",
                    "id": "0djvqMepj2XkHfvWTqkH1N"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Just Close Your Eyes",
                "id": "4fZNHT9xCMtpWbLNrMXH44",
                "uri": "spotify:track:4fZNHT9xCMtpWbLNrMXH44",
                "preview_url": "https://p.scdn.co/mp3-preview/576ae1e455f91aa430ba0428a97bde1a76580e35?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "Lucid Green",
                    "id": "587BLbZ68izUrdPtYAFYfs"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Put",
                "id": "5KPkTyvJ5lYe4AFy8ZDL1A",
                "uri": "spotify:track:5KPkTyvJ5lYe4AFy8ZDL1A",
                "preview_url": "https://p.scdn.co/mp3-preview/b36e07ca455e16149b098f5e81bac472305e4ea8?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Starside Groove",
                "id": "7DGLQE6xCLVtlXiNZP00ee",
                "uri": "spotify:track:7DGLQE6xCLVtlXiNZP00ee",
                "preview_url": "https://p.scdn.co/mp3-preview/756b62f63551b603e81506c4cce68b50cc1bb6db?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mondo Loops",
                    "id": "1XFN3VcuKr4tsTtQlRiTgK"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Drifter",
                "id": "6Lz9vAliJFcu70J4OPmwLW",
                "uri": "spotify:track:6Lz9vAliJFcu70J4OPmwLW",
                "preview_url": "https://p.scdn.co/mp3-preview/1deeaec377d664c64f077d629b78ddfe53f47fcc?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "C4C",
                    "id": "5jZeLexrrwGNUy6nv7tzdr"
                  },
                  {
                    "name": "kokoro",
                    "id": "1uJZwuCV2hXbQDSdJvj198"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Cradle",
                "id": "4ditOLMbGYge1HhSjBvRE4",
                "uri": "spotify:track:4ditOLMbGYge1HhSjBvRE4",
                "preview_url": "https://p.scdn.co/mp3-preview/8c670fd2769e8266d76d73375ef33f5db95c91ef?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "HM Surf",
                    "id": "6TeBxtluBMQixZcKkJ3ZrB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Afloat Again",
                "id": "4OH2CGihah4f9f9QNkyK5v",
                "uri": "spotify:track:4OH2CGihah4f9f9QNkyK5v",
                "preview_url": "https://p.scdn.co/mp3-preview/04e4c02b8d4ad76b2a136caa6d7bd97b2a39488f?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "mell-ø",
                    "id": "6bA2OonnJsG1tN9yClu2aC"
                  },
                  {
                    "name": "Ambulo",
                    "id": "6sPQwc6lix6K1Gv64v91Ml"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Purple Vision",
                "id": "7murAWWK27piC9AvGVOWNv",
                "uri": "spotify:track:7murAWWK27piC9AvGVOWNv",
                "preview_url": "https://p.scdn.co/mp3-preview/0c7cf02eec41ac53520283783ae6edf8acb6c020?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "In Clover",
                "id": "5Tw5Wvgjwd2xN8jQrUpqUu",
                "uri": "spotify:track:5Tw5Wvgjwd2xN8jQrUpqUu",
                "preview_url": "https://p.scdn.co/mp3-preview/40d447db66b0dc1335ba58cc84332fd7b61f014c?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "HM Surf",
                    "id": "6TeBxtluBMQixZcKkJ3ZrB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Mariana",
                "id": "2QfcKK1ffD7NQpTpgnOFxa",
                "uri": "spotify:track:2QfcKK1ffD7NQpTpgnOFxa",
                "preview_url": "https://p.scdn.co/mp3-preview/df0a971ff3433fed8de78157ce7f7ab703b6f149?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "ENRA",
                    "id": "1jDbZQQs4VNtiC4AerpIg4"
                  },
                  {
                    "name": "Sleepermane",
                    "id": "4gGsx7blPpBj7gKGmDBEfI"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Snowman",
                "id": "5ehVOwEZ1Q7Ckkdtq0dY1W",
                "uri": "spotify:track:5ehVOwEZ1Q7Ckkdtq0dY1W",
                "preview_url": "https://p.scdn.co/mp3-preview/b4762770e66b668fc303bee8c6155721d563df10?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "WYS",
                    "id": "2CiO7xWdwPMDlVwlt9qa1f"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Natural Ways",
                "id": "7I72CFQKO0zlEY1wp2Z7ID",
                "uri": "spotify:track:7I72CFQKO0zlEY1wp2Z7ID",
                "preview_url": "https://p.scdn.co/mp3-preview/dc64fbca67c614aed2ac7b1d2a8d5159b75d6500?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Valentine",
                "id": "3jTVk3yYzJ1QMkymFjIjid",
                "uri": "spotify:track:3jTVk3yYzJ1QMkymFjIjid",
                "preview_url": "https://p.scdn.co/mp3-preview/28c5c88ee6c91f93b3d5b7e44a00fa6e68ce1362?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Harmonies",
                "id": "7erOpCDeirqONMpwSCNlVk",
                "uri": "spotify:track:7erOpCDeirqONMpwSCNlVk",
                "preview_url": "https://p.scdn.co/mp3-preview/8027a71992d25ca10494e543c08bd334ab930e29?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Casiio",
                    "id": "5zUSfxfP1NETZiaWt0Ui0a"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Kodamas Path",
                "id": "43g35vGz33dmMPl9iZe1Nd",
                "uri": "spotify:track:43g35vGz33dmMPl9iZe1Nd",
                "preview_url": "https://p.scdn.co/mp3-preview/b31f45ab9fb4be57f2dd45549134781ad659bf6b?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Flitz&Suppe",
                    "id": "49WbztFLx9iDTh8LHEIF84"
                  },
                  {
                    "name": "Mr. Käfer",
                    "id": "79U1adgS3jIlP28wwMHqGJ"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Now & Then",
                "id": "6U3uCdgSvBCe3YYQeR8uyM",
                "uri": "spotify:track:6U3uCdgSvBCe3YYQeR8uyM",
                "preview_url": "https://p.scdn.co/mp3-preview/1a14f4a0ae20f80a0772aac4db791dcce6619e5d?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "ENRA",
                    "id": "1jDbZQQs4VNtiC4AerpIg4"
                  },
                  {
                    "name": "Sleepermane",
                    "id": "4gGsx7blPpBj7gKGmDBEfI"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Satellite",
                "id": "3HdzKbxmMikjtyxWQUWNtK",
                "uri": "spotify:track:3HdzKbxmMikjtyxWQUWNtK",
                "preview_url": "https://p.scdn.co/mp3-preview/5b03ee81a8777076c75fc68e2650ac166f4abc82?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "WYS",
                    "id": "2CiO7xWdwPMDlVwlt9qa1f"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Lost Thoughts",
                "id": "3LTgBTKwl9H5lKMUOKF9Pb",
                "uri": "spotify:track:3LTgBTKwl9H5lKMUOKF9Pb",
                "preview_url": "https://p.scdn.co/mp3-preview/9415a5415f1fac79749181d4cbefa192b697af05?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Fatb",
                    "id": "39c9r5tc96HMSEX9BbxE4T"
                  },
                  {
                    "name": "ZENDR",
                    "id": "71DwiHPKOnvr0mrJKovKcm"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Cold",
                "id": "69kdxhdyEE3464c3BjNvGW",
                "uri": "spotify:track:69kdxhdyEE3464c3BjNvGW",
                "preview_url": "https://p.scdn.co/mp3-preview/3da919c30acb55f100c53c55d7108ba466a57a1b?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Warm Inside",
                "id": "1e47PmVUGgQFoeledbPKY4",
                "uri": "spotify:track:1e47PmVUGgQFoeledbPKY4",
                "preview_url": "https://p.scdn.co/mp3-preview/15d98c67a1df78205719554c9dd6bcc23ac3fbcb?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "softy",
                    "id": "0wcen0V8FgQu6xYupnZMbB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Sunday Fog",
                "id": "3PgU3cywMIrqswJ9A3PGFe",
                "uri": "spotify:track:3PgU3cywMIrqswJ9A3PGFe",
                "preview_url": "https://p.scdn.co/mp3-preview/bd466b536c31a9ebecd99e1468034c0e4eac5bd9?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "goosetaf",
                    "id": "46NCqFl8vhQZD77y7XkvJs"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Thoughts of You",
                "id": "03mOTxo1RucacNpaWpv6gE",
                "uri": "spotify:track:03mOTxo1RucacNpaWpv6gE",
                "preview_url": "https://p.scdn.co/mp3-preview/8d54a8908da501f878f6654db8a681ee7db25cec?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Spencer Hunt",
                    "id": "4btBTQ1pWqpnDPY4BWMh1S"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Mujinas Ramen Shop",
                "id": "4xYKlVprgM9DtyAEjk3w8j",
                "uri": "spotify:track:4xYKlVprgM9DtyAEjk3w8j",
                "preview_url": "https://p.scdn.co/mp3-preview/89685c1e98fd4453dc77beded014812c22e6c891?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Flitz&Suppe",
                    "id": "49WbztFLx9iDTh8LHEIF84"
                  },
                  {
                    "name": "Mr. Käfer",
                    "id": "79U1adgS3jIlP28wwMHqGJ"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Lullaby",
                "id": "5qfcbBSvtHqnCFkvd2YTps",
                "uri": "spotify:track:5qfcbBSvtHqnCFkvd2YTps",
                "preview_url": "https://p.scdn.co/mp3-preview/13239923a74e9931650df230b8e6144bb5228221?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Team Astro",
                    "id": "5BJiJign2sqMUCJhpStgNd"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Stay the Same",
                "id": "3TACMcYZKWofQbtoek5X8t",
                "uri": "spotify:track:3TACMcYZKWofQbtoek5X8t",
                "preview_url": "https://p.scdn.co/mp3-preview/c33ab6df3dd3ea88741ce30be5e73362590fe406?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "mell-ø",
                    "id": "6bA2OonnJsG1tN9yClu2aC"
                  },
                  {
                    "name": "Ambulo",
                    "id": "6sPQwc6lix6K1Gv64v91Ml"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Espresso",
                "id": "2ePOkNgfuu4UY8CCsnBDcW",
                "uri": "spotify:track:2ePOkNgfuu4UY8CCsnBDcW",
                "preview_url": "https://p.scdn.co/mp3-preview/9448dbc1c4b17316c3b2423c0f63af1f614e3484?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Aso",
                    "id": "45Ui3GdcxzbdJhhTtZLXO8"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Lost",
                "id": "2gIJkf741uNiE1mnHKmioQ",
                "uri": "spotify:track:2gIJkf741uNiE1mnHKmioQ",
                "preview_url": "https://p.scdn.co/mp3-preview/1cdb7344be0cb2337db9c78a1ccf6936913b150a?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kanisan",
                    "id": "0Q6S7QIOyuvDYzbhpvM5FO"
                  },
                  {
                    "name": "no one's perfect",
                    "id": "4vXVzSSH673xUv5sUmRGYX"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Ruby",
                "id": "5zge7yrQndqV5tWlChQPCm",
                "uri": "spotify:track:5zge7yrQndqV5tWlChQPCm",
                "preview_url": "https://p.scdn.co/mp3-preview/a78a9f94b03b0428c6fd7db13bd5370d444c23d3?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Sleepermane",
                    "id": "4gGsx7blPpBj7gKGmDBEfI"
                  },
                  {
                    "name": "Sebastian Kamae",
                    "id": "7GsvnIE0bUBu6WZXO3ryJe"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Take Me Back",
                "id": "69ScUfmrwMZgyqb0XRGw12",
                "uri": "spotify:track:69ScUfmrwMZgyqb0XRGw12",
                "preview_url": "https://p.scdn.co/mp3-preview/cb99029c2c2def61ba3b0a920a6529c8fecb172e?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "WYS",
                    "id": "2CiO7xWdwPMDlVwlt9qa1f"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Snow & Sand",
                "id": "55qrhJbNJBwtCRagmXK9lI",
                "uri": "spotify:track:55qrhJbNJBwtCRagmXK9lI",
                "preview_url": "https://p.scdn.co/mp3-preview/27f47e78bd8b4b0beca2ec805dc0ba96d2722c8b?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "j'san",
                    "id": "5iMUho98faEp2w6j5p44PH"
                  },
                  {
                    "name": "Epektase",
                    "id": "31jYTsfmnHqcK7ahdqlqmo"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "A Walk in the Park",
                "id": "2njn3Dr0edrc97PAMoSYBR",
                "uri": "spotify:track:2njn3Dr0edrc97PAMoSYBR",
                "preview_url": "https://p.scdn.co/mp3-preview/3c4d68a74f3f01a5590b708119f636d98817550e?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Laffey",
                    "id": "7LWdcPFBFcRaamGjIJbPV7"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Back Home",
                "id": "7FuzdjVf4yXdrb8hbKdiv2",
                "uri": "spotify:track:7FuzdjVf4yXdrb8hbKdiv2",
                "preview_url": "https://p.scdn.co/mp3-preview/3460a8e00ff0b619c7fae3d357dee68587200288?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Casiio",
                    "id": "5zUSfxfP1NETZiaWt0Ui0a"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Deep Down",
                "id": "2u2143VCfJPt6RW3Ti7Kb4",
                "uri": "spotify:track:2u2143VCfJPt6RW3Ti7Kb4",
                "preview_url": "https://p.scdn.co/mp3-preview/26f520913a1b9483fb10fa3bec2745ac57d6d3e0?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Pandrezz",
                    "id": "65ZGdYSRT3Rmv6P7DN4XCC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Breeze",
                "id": "0rwnpmJr7XP4CfQxE5vYCy",
                "uri": "spotify:track:0rwnpmJr7XP4CfQxE5vYCy",
                "preview_url": "https://p.scdn.co/mp3-preview/c751eab3c61e1c2b8e1eec1b033337ddf23964b1?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mvdb",
                    "id": "0hleYpwrJSPEh2pCRTU0AY"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Reflection",
                "id": "7JHDwKBqF6375JdMLRwGlK",
                "uri": "spotify:track:7JHDwKBqF6375JdMLRwGlK",
                "preview_url": "https://p.scdn.co/mp3-preview/f7bf0b97fc222d341d8c1cbd1cb5b722571c7699?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Icicles",
                "id": "5LprvdAcmuE3XNsMqyoyM7",
                "uri": "spotify:track:5LprvdAcmuE3XNsMqyoyM7",
                "preview_url": "https://p.scdn.co/mp3-preview/26ea17d2d6b4eaa2bdb92dbfe8eb0106994d59f9?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "G Mills",
                    "id": "0djvqMepj2XkHfvWTqkH1N"
                  },
                  {
                    "name": "Chris Mazuera",
                    "id": "3Sb3oI3Xw7FcgYS262zXPE"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Sun",
                "id": "12p9iameBtppGCoMPlLVxz",
                "uri": "spotify:track:12p9iameBtppGCoMPlLVxz",
                "preview_url": "https://p.scdn.co/mp3-preview/32634d1721371f4ccbcc060ac429b03916e66e1c?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Comforting You",
                "id": "4MuNaeIxOBvfJqmIUoX5aU",
                "uri": "spotify:track:4MuNaeIxOBvfJqmIUoX5aU",
                "preview_url": "https://p.scdn.co/mp3-preview/bda389deb5eac1c0e70864d0a98b5219158ad1a4?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "WYS",
                    "id": "2CiO7xWdwPMDlVwlt9qa1f"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Through the Clouds",
                "id": "65rgrr0kAPjOkeWA2bDoUQ",
                "uri": "spotify:track:65rgrr0kAPjOkeWA2bDoUQ",
                "preview_url": "https://p.scdn.co/mp3-preview/2341e921a96b04d87c02f24db821c684508ace33?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "brillion.",
                    "id": "77aATgrzmuoRjmqxPcEIHU"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Autumn Morning",
                "id": "1zqkbok4NBWh5UHbQi6jmw",
                "uri": "spotify:track:1zqkbok4NBWh5UHbQi6jmw",
                "preview_url": "https://p.scdn.co/mp3-preview/a3ba5ae8a0a754c424f2add1320315714e631553?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "softy",
                    "id": "0wcen0V8FgQu6xYupnZMbB"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Ato",
                "id": "3425s5AtXBR3W3asGco0Tj",
                "uri": "spotify:track:3425s5AtXBR3W3asGco0Tj",
                "preview_url": "https://p.scdn.co/mp3-preview/4fd143369e135181bc43d49f4d0ac59c114c9b58?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Flitz&Suppe",
                    "id": "49WbztFLx9iDTh8LHEIF84"
                  },
                  {
                    "name": "Mr. Käfer",
                    "id": "79U1adgS3jIlP28wwMHqGJ"
                  },
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Pendulum",
                "id": "1UW2cDI4KcNn2Zp0NVBIYV",
                "uri": "spotify:track:1UW2cDI4KcNn2Zp0NVBIYV",
                "preview_url": "https://p.scdn.co/mp3-preview/149808df6b0f8bce659be77c340172a731ac2a08?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kanisan",
                    "id": "0Q6S7QIOyuvDYzbhpvM5FO"
                  },
                  {
                    "name": "no one's perfect",
                    "id": "4vXVzSSH673xUv5sUmRGYX"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Steps",
                "id": "0e0BS4zwbHHI5jTS6pSlJg",
                "uri": "spotify:track:0e0BS4zwbHHI5jTS6pSlJg",
                "preview_url": "https://p.scdn.co/mp3-preview/12ffa8856520bcabefe3ce57e4259c1b8559cb40?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "dryhope",
                    "id": "50Ej4gF8iYESted3e4JZ4t"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Winter Shells",
                "id": "76pZiU5vU5OrRY0PPleJj1",
                "uri": "spotify:track:76pZiU5vU5OrRY0PPleJj1",
                "preview_url": "https://p.scdn.co/mp3-preview/efdb53824767ede00a7967de8faa903150a8a531?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mondo Loops",
                    "id": "1XFN3VcuKr4tsTtQlRiTgK"
                  },
                  {
                    "name": "Kanisan",
                    "id": "0Q6S7QIOyuvDYzbhpvM5FO"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Epilogue",
                "id": "5zZY5LXTgeTmuPMR0bTgJW",
                "uri": "spotify:track:5zZY5LXTgeTmuPMR0bTgJW",
                "preview_url": "https://p.scdn.co/mp3-preview/dc2dba57700efdea708f674656b3974d644d23dd?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "mell-ø",
                    "id": "6bA2OonnJsG1tN9yClu2aC"
                  },
                  {
                    "name": "Ambulo",
                    "id": "6sPQwc6lix6K1Gv64v91Ml"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Waves Calling",
                "id": "0uHgi8yg7NEQVIDvohLUlu",
                "uri": "spotify:track:0uHgi8yg7NEQVIDvohLUlu",
                "preview_url": "https://p.scdn.co/mp3-preview/3059da9f99b598fda97343543ac2f9300737edd8?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Mondo Loops",
                    "id": "1XFN3VcuKr4tsTtQlRiTgK"
                  },
                  {
                    "name": "Kanisan",
                    "id": "0Q6S7QIOyuvDYzbhpvM5FO"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Slow",
                "id": "1wAcunzpQa7gwac80HXohs",
                "uri": "spotify:track:1wAcunzpQa7gwac80HXohs",
                "preview_url": "https://p.scdn.co/mp3-preview/f86a6e49d8079a416c3d6512f0ad39086c5a8c66?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "kudo",
                    "id": "2Gq799yTibgF2YWweyB16G"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Magic",
                "id": "5A1pofoPtUfeoYbgD9K15F",
                "uri": "spotify:track:5A1pofoPtUfeoYbgD9K15F",
                "preview_url": "https://p.scdn.co/mp3-preview/0494da6ccb6641256985253ebd9b088620c011e7?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Kupla",
                    "id": "7daSp9zXk1dmqNxwKFkL35"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Backpack City",
                "id": "6Fv9r2ov4aWPArkb5cKIZR",
                "uri": "spotify:track:6Fv9r2ov4aWPArkb5cKIZR",
                "preview_url": "https://p.scdn.co/mp3-preview/1742b95bb26c5fa4d55b237830c70c8ca64ab7c0?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "Flovry",
                    "id": "2pLu3Ut2C3RviYZ3xUanBs"
                  },
                  {
                    "name": "tender spring",
                    "id": "0WCCipy2qiobvuygnTMdkC"
                  }
                ]
              }
            },
            {
              "track": {
                "name": "Ease Out",
                "id": "5cyZufWEEbEpZgpqqDCA5x",
                "uri": "spotify:track:5cyZufWEEbEpZgpqqDCA5x",
                "preview_url": "https://p.scdn.co/mp3-preview/88844bada24d48c84159552d14c2558ac4636950?cid=c5c6ad9baec948779f7413dbc1fdae80",
                "artists": [
                  {
                    "name": "softy",
                    "id": "0wcen0V8FgQu6xYupnZMbB"
                  }
                ]
              }
            }
          ]
        }
      ]
    }
  }
}