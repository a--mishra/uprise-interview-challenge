import React from 'react'
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';



const useStyles = makeStyles(theme => ({

  formControl: {
    minWidth: 400, 
  },
  option: {
    fontSize:12,
    color:'#333333'
  },
  
  setBorder: {
    height:62,
    marginLeft:5
  },
  labelsize: {
      fontSize:10
  },
  optionsize: {
    fontSize: 12,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  selectheight: {
    height:62,
  },
  optionHeight: {
    margin:5
  }  
}));

const optionsList = [
  { id: '1', title: 'Campaign 1'},
  { id: '2', title: 'Campaign 2' },
  { id: '3', title: 'Campaign 3' },
  { id: '4', title: 'Campaign 4'},
  { id: '5', title: 'Campaign 5'},
  { id: '6', title: 'Campaign 6'},
  { id: '7', title: 'Campaign 7'},
  { id: '8', title: 'Campaign 8'},
  { id: '9', title: 'Campaign 9'},
  { id: '10', title: 'Campaign 10'}
];

export default function QuotaSelect(){
  // const [value, setValue] = React.useState({
  //   value: '',
  // });
  const classes = useStyles();

  const defaultProps = {
    options: optionsList,
    getOptionLabel: option => option.title,
  
  };

  const flatProps = {
    options: optionsList.map(option => option.title),
  };

  const [value, setValue] = React.useState({
    id:''
  });

  const onOptionSelected= (event, values) => {
    
  } 

  

  return (    

        <FormControl className={classes.formControl}>
        <Autocomplete
          {...defaultProps}
          id="clear-on-escape"
          className="selectDisposition"
          classes={{
            option: classes.option,
          }}
          onChange={onOptionSelected}
          renderInput={params => (
            <TextField {...params} margin="normal" fullWidth  />
          )}
        />
      </FormControl>
    
  );
}
