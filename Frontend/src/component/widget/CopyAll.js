
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';

const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
    '&:hover':{
        backgroundColor:'tranparent',
        boxShadow:'none'
    }
  },
  btnsize:{
      fontSize:12,
      color:'#333333',
      textTransform:'none',
      margin:0,
      padding:0
  },

  filecolor: {
      color:'#999999',
      fontSize:12,
      padding:6,
      background:'#F2F2F2',
      borderRadius:25,
      marginRight:10
  }
}));

export default function CopyAll() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button className={classes.btnsize}> <FileCopyOutlinedIcon className={classes.filecolor} /> Copy to all</Button>

    </div>
  );
}