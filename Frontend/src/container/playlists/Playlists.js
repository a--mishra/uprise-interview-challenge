import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { Button } from '@uprise/button'
import SimpleImageSlider from "react-simple-image-slider";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1    
  },
  image: {
    // width: 256,
    height: 232,
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
    borderRadius: '8px'
  },
}));

export default function Playlists(props) {
  const classes = useStyles();
  const images = [
      { url: "images/1.jpg", label: "" },
      { url: "images/2.jpg", label: "playlist2" },
      { url: "images/3.jpg", label: "playlist3" },
      { url: "images/4.jpg", label: "playlist4" },
      { url: "images/5.jpg", label: "playlist5" },
      { url: "images/6.jpg", label: "playlist6" },
      { url: "images/7.jpg", label: "playlist7" },
  ];

  let [imgIndex, setImgIndex] = useState(0);

  return (
    <div className={classes.root} className={'profileContainer'}>
        <Grid container spacing={2} direction="column">
          <Grid item className={'centeredContainer'}>
            <SimpleImageSlider
                      width={412}
                      height={232}
                      images={props.images ? props.images : images}
                      slideDuration={0.2}
                      navStyle={2}
                      style={{borderRadius:'8px'}}
                      showBullets={false}
                      bgColor={'#EDEAFA'}
                      onClickNav={(toRight)=>{
                        if(toRight){
                          setImgIndex(imgIndex+1);
                        } else {
                          setImgIndex(imgIndex-1);
                        }
                      }}
                  />
          </Grid>
          <Grid item className={'centeredContainer'}>
                <Typography gutterBottom variant={'h5'} className={'playlistCaption'}>{props.images && props.images[imgIndex] ? props.images[imgIndex].label : images[imgIndex].label}</Typography>
          </Grid>
        </Grid>
    </div>
  );
}
