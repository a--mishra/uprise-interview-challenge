import {combineReducers} from 'redux'
import dataReducer from './dataReducer'
import uiReducer from './uiReducer'
import notifications from './notifications'
import { reducer as formReducer } from 'redux-form'


export const rootReducer = combineReducers({
    dataReducer,
    uiReducer,
    notifications,
    form: formReducer
    });

export default rootReducer