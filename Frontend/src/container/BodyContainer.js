import React, { Component } from 'react'
import {connect} from 'react-redux'
import TopBar from '../container/topBar/TopBar'
import SimpleTabs from '../container/topBar/TopBar'

class BodyContainer extends Component {

  constructor(props) {
    super(props);
    this.state={
      loaded:false
    }
  }

  componentWillMount() {
  }


  render() {
    let tabData = {
      tab1:{
        imgUrl:"https://i.scdn.co/image/ab6775700000ee85cbad6f248c4cc48a076d39a7",
        displayName:"ChilledBeer",
        followersCount:"478,248",
        followCurrentProfile:()=>{
          //alert('Iam clicked')
          }
      },
      tab2:{
        images:[{ url: "images/2.jpg", label: "playlist2" }]
      },
      tab3:{
        artists:["XO","Dune"]
      }
    };


      // fill in the data;;
      if(this.props.profilePlaylists && this.props.profileLoaded == true) {
        try{
          let data = this.props.profilePlaylists.data;
          tabData.tab1.displayName = data.user.display_name;
          tabData.tab1.profileId = data.user.id;
          tabData.tab1.imgUrl = data.user.images[0].url;

          // for playlists
          tabData.tab2.images = [];

          let artistObj = {};

          Object.keys(data.user.playlists).forEach( (key, index) => {
            Object.keys(data.user.playlists[key].images).forEach((key2, index2)=>{
              tabData.tab2.images.push({url:data.user.playlists[key].images[key2].url, label: data.user.playlists[key].name})
            })

            let tracks = data.user.playlists[key].tracks;
            if(tracks){
              tracks.forEach((element) =>{
                let artists = element.track.artists;
                artists.forEach((artist)=>{
                  artistObj[artist.id] = artist.name;
                })
              })
            }
          })

          Object.keys(artistObj).forEach((key, index)=>{
            tabData.tab3.artists.push(artistObj[key]);
          })

          console.log(tabData);

        } catch(error) {
          console.log(error)
        }
      }

      return (
        <div className={'bodyContainer'}>
          <SimpleTabs data={tabData}/>
        </div>
      )
    
  }
}


const mapStateToProps = (state) => ({
  profilePlaylists : state.dataReducer && state.dataReducer.profilePlaylists ? state.dataReducer.profilePlaylists: undefined,
  profileLoaded : state.uiReducer && state.uiReducer.loaded  && state.uiReducer.loaded.profile ? state.uiReducer.loaded.profile: false,  

})

const mapToDispatchProps = (dispatch, ownProps) => ({
})

export default connect(mapStateToProps, mapToDispatchProps)(BodyContainer);

