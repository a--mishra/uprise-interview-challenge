export const setProfilePlaylists= (data) =>{
    return{
        type : 'PROFILE_PLAYLISTS',
        data
    }
}

export const setProfile= (data) =>{
    return{
        type : 'PROFILE',
        data
    }
}

export const setLoaded= (data) =>{
    return{
        type : 'SET_LOADED',
        data
    }
}

