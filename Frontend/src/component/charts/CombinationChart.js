import React, {
    Component
} from 'react';
import {
    render
} from 'react-dom';
import HighchartsReact from 'highcharts-react-official';
import Highcharts from 'highcharts';

class CombinationChart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            // To avoid unnecessary update keep all options in the state.
            chartOptions: {
                title: {
                    text: this.props.title ? this.props.title : 'Detailed Chart'
                },
                xAxis: {
                    categories: this.props.xAxisCategories ? this.props.xAxisCategories : []
                },
                labels: {
                    items: [{
                        html: 'Total Call Conectivity',
                        style: {
                            left: '50px',
                            top: '18px',
                            color: ( // theme
                                Highcharts.defaultOptions.title.style &&
                                Highcharts.defaultOptions.title.style.color
                            ) || 'black'
                        }
                    }]
                },
                series: [{
                    type: 'column',
                    name: this.props.data && this.props.data.series1 && this.props.data.series1.label ? this.props.data.series1.label : 'label1',
                    data: this.props.data && this.props.data.series1 && this.props.data.series1.value ? this.props.data.series1.value : []
                }, {
                    type: 'column',
                    name: this.props.data && this.props.data.series2 && this.props.data.series2.label ? this.props.data.series2.label : 'label2',
                    data: this.props.data && this.props.data.series2 && this.props.data.series2.value ? this.props.data.series2.value : []
                }, {
                    type: 'column',
                    name: this.props.data && this.props.data.series3 && this.props.data.series3.label ? this.props.data.series3.label : 'label3',
                    data: this.props.data && this.props.data.series3 && this.props.data.series3.value ? this.props.data.series3.value : []
                }, {
                    type: 'column',
                    name: this.props.data && this.props.data.series4 && this.props.data.series4.label ? this.props.data.series4.label : 'label4',
                    data: this.props.data && this.props.data.series4 && this.props.data.series4.value ? this.props.data.series4.value : []
                }, {
                    type: 'column',
                    name: this.props.data && this.props.data.series5 && this.props.data.series5.label ? this.props.data.series5.label : 'label5',
                    data: this.props.data && this.props.data.series5 && this.props.data.series5.value ? this.props.data.series5.value : []
                }, {
                    type: 'column',
                    name: this.props.data && this.props.data.series6 && this.props.data.series6.label ? this.props.data.series6.label : 'label6',
                    data: this.props.data && this.props.data.series6 && this.props.data.series6.value ? this.props.data.series6.value : []
                }, {
                    type: 'column',
                    name: this.props.data && this.props.data.series7 && this.props.data.series7.label ? this.props.data.series7.label : 'label7',
                    data: this.props.data && this.props.data.series7 && this.props.data.series7.value ? this.props.data.series7.value : []
                }, {
                    type: 'column',
                    name: this.props.data && this.props.data.series8 && this.props.data.series8.label ? this.props.data.series8.label : 'label8',
                    data: this.props.data && this.props.data.series8 && this.props.data.series8.value ? this.props.data.series8.value : []
                }, {
                    type: 'spline',
                    name: this.props.data && this.props.data.series9 && this.props.data.series9.label ? this.props.data.series9.label : 'label9',
                    data: this.props.data && this.props.data.series9 && this.props.data.series9.value ? this.props.data.series9.value : [],
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }
                }, {
                    type: 'pie',
                    name: 'Today\'s Total',
                    data: [{
                        name: this.props.data && this.props.data.aggregated && this.props.data.aggregated[0] && this.props.data.aggregated[0].label ? this.props.data.aggregated[0].label : 'label1',
                        y:  this.props.data && this.props.data.aggregated && this.props.data.aggregated[0] && this.props.data.aggregated[0].value ? this.props.data.aggregated[0].value : 0,
                        color: Highcharts.getOptions().colors[0] 
                    }, {
                        name: this.props.data && this.props.data.aggregated && this.props.data.aggregated[1] && this.props.data.aggregated[1].label ? this.props.data.aggregated[1].label : 'label2',
                        y: this.props.data && this.props.data.aggregated && this.props.data.aggregated[1] && this.props.data.aggregated[1].value ? this.props.data.aggregated[1].value : 0,
                        color: Highcharts.getOptions().colors[1] 
                    }, {
                        name: this.props.data && this.props.data.aggregated && this.props.data.aggregated[2] && this.props.data.aggregated[2].label ? this.props.data.aggregated[2].label : 'label3',
                        y: this.props.data && this.props.data.aggregated && this.props.data.aggregated[2] && this.props.data.aggregated[2].value ? this.props.data.aggregated[2].value : 0,
                        color: Highcharts.getOptions().colors[2] 
                    }, {
                        name: this.props.data && this.props.data.aggregated && this.props.data.aggregated[3] && this.props.data.aggregated[3].label ? this.props.data.aggregated[3].label : 'label4',
                        y: this.props.data && this.props.data.aggregated && this.props.data.aggregated[3] && this.props.data.aggregated[3].value ? this.props.data.aggregated[3].value : 0,
                        color: Highcharts.getOptions().colors[3] 
                    }, {
                        name: this.props.data && this.props.data.aggregated && this.props.data.aggregated[4] && this.props.data.aggregated[4].label ? this.props.data.aggregated[4].label : 'label5',
                        y: this.props.data && this.props.data.aggregated && this.props.data.aggregated[4] && this.props.data.aggregated[4].value ? this.props.data.aggregated[4].value : 0,
                        color: Highcharts.getOptions().colors[4] 
                    }, {
                        name: this.props.data && this.props.data.aggregated && this.props.data.aggregated[5] && this.props.data.aggregated[5].label ? this.props.data.aggregated[5].label : 'label6',
                        y: this.props.data && this.props.data.aggregated && this.props.data.aggregated[5] && this.props.data.aggregated[5].value ? this.props.data.aggregated[5].value : 0,
                        color: Highcharts.getOptions().colors[5] 
                    }, {
                        name: this.props.data && this.props.data.aggregated && this.props.data.aggregated[6] && this.props.data.aggregated[6].label ? this.props.data.aggregated[6].label : 'label7',
                        y: this.props.data && this.props.data.aggregated && this.props.data.aggregated[6] && this.props.data.aggregated[6].value ? this.props.data.aggregated[6].value : 0,
                        color: Highcharts.getOptions().colors[6] 
                    }],
                    center: [100, 80],
                    size: 100,
                    showInLegend: false,
                    dataLabels: {
                        enabled: false
                    }
                }]
            },
            hoverData: null
        };
    }


    setHoverData = (e) => {
        // The chart is not updated because `chartOptions` has not changed.
        this.setState({
            hoverData: e.target.category
        })
    }


    render() {
        const {
            chartOptions,
            hoverData
        } = this.state;

        return ( <div >
                    <HighchartsReact highcharts = {Highcharts} options = {chartOptions}/> 
                    {/* <h3 > Hovering over {hoverData} </h3> 
                    <button onClick = {
                        this.updateSeries.bind(this)
                    } > Update Series </button>  */}
                </div >
        )
    }
}

export default CombinationChart