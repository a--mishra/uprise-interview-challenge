import React, { Component } from 'react';
import {connect} from 'react-redux'
import MainContainer from '../src/container/MainContainer'
import { fetchInitialData, fetchPollingData, fetchMasterData } from './middlewares/Middleware'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      };
    }

  componentDidMount() {
    this.props.fetchInitialData();
    this.props.fetchPollingData();
    this.props.fetchMasterData();
    this.timer1 = setInterval(this.props.fetchPollingData, window.settings('pollingInterval') &&  window.settings('pollingInterval') > 10000 ? window.settings('pollingInterval') : 10000);
    this.timer2 = setInterval(this.props.fetchMasterData, window.settings('dataFetchInterval') &&  window.settings('dataFetchInterval') > 30000 ? window.settings('dataFetchInterval') : 30000);
  }

  componentWillUnmount() {
    clearInterval(this.timer1);
    clearInterval(this.timer2);
  }

  render() {
      return (
        <>
          <MainContainer />
        </>
      );
    }

}


const mapStateToProps = (state) => ({
})


const mapToDispatchProps = (dispatch, ownProps) => ({
  fetchInitialData: () => {
    dispatch(fetchInitialData())
  },
  fetchPollingData: () => {
    dispatch(fetchPollingData())
  },
  fetchMasterData : () => {
    dispatch(fetchMasterData())
  }
})



export default connect(mapStateToProps, mapToDispatchProps)(App);

