import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import {Drawer, Collapse, Container, IconButton, Button} from '@material-ui/core'
import {Close} from '@material-ui/icons'
import Spacer from '../Spacer/Spacer'

const useStyles = makeStyles(theme => ({
  drawerPaper: {
    // minWidth: theme.spacing(60)
    minWidth: '100px',
    width: '25vw',
    maxWidth: '500px'
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: '100%'
  },
  content: {
    display: 'flex',
    justifyContent: 'center',
    maxWidth: '100%',
    flexDirection: 'column'
  },
  footer: {
    display: 'flex',
    alignItems: 'start',
    maxWidth: '100%'
  },
  container: {
    height: '100%',
    padding: theme.spacing(3)
  },
  marginLeft: {
    marginLeft: theme.spacing(1)
  }
}))

function RightDrawer(props) {
  const classes = useStyles()
  const {open, onClose, children, action, actionLabel, getTranslation} = props

  return (
    <Collapse in={open}>
      <Drawer anchor='right' open={open} onClose={onClose} classes={{paper: classes.drawerPaper}}>
        <Container className={classes.container}>
          <div className={classes.container}>
            <div className={classes.header}>
              <Spacer />
              <div>
                <IconButton onClick={onClose}>
                  <Close fontSize='large' />
                </IconButton>
              </div>
            </div>
            <div className={classes.content}>
              {children}
            </div>
            {action && (
              <div className={classes.footer}>
                <Button size='large' variant='contained' className={classes.marginLeft} fullWidth color='secondary' onClick={action || null}>
                  {actionLabel || 'Action button'}
                </Button>
              </div>
            )}
          </div>
        </Container>
      </Drawer>
    </Collapse>
  )
}

RightDrawer.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  getTranslation: PropTypes.func.isRequired,
  action: PropTypes.func,
  actionLabel: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
}

export default RightDrawer
