        //---------------------------------------------------------------------------------------------------------------------
        // Standarized : Console Logging
        //---------------------------------------------------------------------------------------------------------------------
        var configuration = {};

        var request = new XMLHttpRequest();
        request.open('GET', '../manifest.json', false);  // `false` makes the request synchronous
        request.send(null);

        if (request.status === 200) {
            log(request, "App Configuration Result", 'Debug',1);

            configuration = JSON.parse(request.responseText);
            log(configuration, "App Configuration", 'Debug',1);
        } else {
            configuration = {
                "name":"dishtv_dashboard_1",
                "version":"0.0.0"
            };
            log(configuration, "App Configuration", 'Debug');
        }

        if(configuration.name == undefined){
            configuration = {
                "name":"dishtv_dashboard_1",
                "version":"0.0.0"
            }
        }

        function log(message, description = '',level = 'Debug', theme = 0) {
            let name = configuration.name;
            let version = configuration.version;
            let lineBreak = '-----------------------------------------------------'

            let logData = {
                "App Name": name,
                "App Version": version,
                "Log Date": getCurrentTime(),
                "Description": description,
                "Message" : message
            };


            let css = 'color: Black; font-size:1.2em; font-family:monospace';
            switch (theme) {
                case 0:  
                    // do nothing;
                    break;
                case 1:     
                    css = 'color: Black; background: yellow; font-size:1.2em;';
                    break;
                case 2:   
                    css = 'color: Red; background: #c7ebf0; font-size:1.2em;';
                    break;
                case 3:  
                    css = 'color: Red; background: Black; font-size:1.2em;';
                    break;
                default: 
                    // do nothing
            }


            let enviroment = configuration.env == undefined ? 'DEV' : configuration.env;
            if(enviroment == 'PROD'){
                logData = JSON.stringify(logData)
                switch (level) {
                    case 'Debug':  
                    console.debug(`%c${description}\n${lineBreak}\n${logData}`, `${css}`);
                    break

                    case 'Info':     
                    console.info(`%c${description}\n${lineBreak}\n${logData}`, `${css}`);
                    break;
                    
                    case 'Warn':   
                    console.warn(`%c${description}\n${lineBreak}\n${logData}`, `${css}`);
                    break;

                    case 'Error':  
                    console.error(`%c${description}\n${lineBreak}\n${logData}`, `${css}`);
                    break;

                    default: 
                    console.log(`%c${description}\n${lineBreak}\n${logData}`, `${css}`);
                }
            } else {
                switch (level) {
                    case 'Debug':  
                    console.debug(`${description}\n${lineBreak}\n`,logData);
                    break

                    case 'Info':     
                    console.info(`${description}\n${lineBreak}\n`,logData);
                    break;
                    
                    case 'Warn':   
                    console.warn(`${description}\n${lineBreak}\n`,logData);
                    break;

                    case 'Error':  
                    console.error(`${description}\n${lineBreak}\n`,logData);
                    break;

                    default: 
                    console.log(`${description}\n${lineBreak}\n`,logData);
                }
            }
        }

        function getCurrentTime() {
        var date = new Date();
        var dateStr =
            date.getFullYear() + "-" +
            ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
            ("00" + date.getDate()).slice(-2) + " " +
            ("00" + date.getHours()).slice(-2) + ":" +
            ("00" + date.getMinutes()).slice(-2) + ":" +
            ("00" + date.getSeconds()).slice(-2) + "." +
            ("000" + date.getMilliseconds()).slice(-3);
        return dateStr;
        }

    //---------------------------------------------------------------------------------------------------------------------
