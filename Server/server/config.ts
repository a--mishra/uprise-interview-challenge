const config = {
    server: {
        sessionSecret: '',
        port: 4000,
        mountPath: '/'
    },
    spotify: {
        clientId: '',
        clientSecret: '',
        scopes: ['user-library-read','playlist-read-private','playlist-read-collaborative','user-read-private','user-read-email','user-follow-read','user-top-read', 'user-read-playback-state'],
        redirectUri: 'http://localhost:4000/auth/callback'
    }
};

export { config };
