import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Backdrop from '@material-ui/core/Backdrop';
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import FileCopyIcon from '@material-ui/icons/FileCopyOutlined';
import SaveIcon from '@material-ui/icons/Save';
import PrintIcon from '@material-ui/icons/Print';
import ShareIcon from '@material-ui/icons/Share';
import FavoriteIcon from '@material-ui/icons/Favorite';
import RestoreIcon from '@material-ui/icons/Restore';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import 'status-indicator/styles.css' 

const useStyles = makeStyles((theme) => ({
  root: {
    // height: 0,
    // transform: 'translateZ(0px)',
    // flexGrow: 1,
    // position: 'relative',
    // bottom:'10px'
    position: 'absolute',
    bottom: '16px',
    right: '16px',

  },
  speedDial: {
    // position: 'absolute',
    // bottom: theme.spacing(2),
    // right: theme.spacing(2),
  },
  backDrop: {
  }
}));

// const actions = [
//   { icon: <SaveIcon />, name: 'Save' },
//   { icon: <RestoreIcon />, name: 'Restore' },
//   { icon: <FileCopyIcon />, name: 'Copy' },
//   { icon: <DeleteForeverIcon />, name: 'Delete' },  
// ];

export default function SpeedDialTooltipOpen(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [hidden, setHidden] = React.useState(false);
  const actions = props.actions != undefined ? props.actions : [
    { icon: <SaveIcon />, name: 'Save' },
    { icon: <RestoreIcon />, name: 'Restore' },
    { icon: <FileCopyIcon />, name: 'Copy' },
    { icon: <DeleteForeverIcon />, name: 'Delete' },  
  ];

  const handleVisibility = () => {
    setHidden((prevHidden) => !prevHidden);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.root}>
      {/* <Backdrop className={classes.backDrop} open={open} /> */}

      <SpeedDial
        ariaLabel="SpeedDial tooltip example"
        className={classes.speedDial}
        hidden={hidden}
        icon={<SpeedDialIcon />}
        onClose={handleClose}
        onOpen={handleOpen}
        open={open}
        FabProps={{
          size:'small',
          // color:'default',
          style:{backgroundColor:'#182C4C'}
        }}
      >
        {actions.map((action) => (
          <SpeedDialAction
            key={action.name}
            icon={action.icon}
            tooltipTitle={action.name}
            tooltipOpen
            onClick={handleClose}
          />
        ))}
      </SpeedDial>
      {
        // props.active && <div className="filterSelected"></div>
        /* active | positive | intermediary | negative | pulse */
        props.active && <status-indicator negative pulse 
          style={{
            right: '11px',
            bottom: '29px',
            position: 'absolute',
          }}></status-indicator>
      }
    </div>
  );
}
