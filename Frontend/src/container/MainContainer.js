import React, { Component } from 'react'
import {connect} from 'react-redux'
import { PulseLoader } from 'react-spinners';
import { css } from '@emotion/core';
import StickyFooter from '../component/footer/StickyFooter'
import BodyContainer from './BodyContainer'
import { Loader } from '@uprise/loader'

const override = css`
    display: block;
    margin: 0 auto;
    border-color: #11569C;
    padding-top: 48vh;
    padding-bottom: 2vh;`;

class MainContainer extends Component {
  constructor(props) {
    super(props);
    this.state={
      loaded:false
    }
  }

  componentWillMount() {
  }

  render() {
    if(this.props.profileLoaded) {

      return (
            <div className="bodyContainerWrapper">
              <BodyContainer />
            </div>
      )
    } else {
      return(
        <Loader />

        // <div>
        //   <div className={`message`}>
        //     <PulseLoader
        //       css={override}
        //       sizeUnit={"px"}
        //       size={15}
        //       color={'#11569C'}
        //       loaded={true}
        //       />
        //   </div>
        //   <div className={`message`}>
        //     <p>
        //       Loading...
        //     </p>
        //   </div>
        // </div>
      )
    }
  }
}


const mapStateToProps = (state) => ({
  profileLoaded : state.uiReducer && state.uiReducer.loaded  && state.uiReducer.loaded.profile ? state.uiReducer.loaded.profile: false,  
})

const mapToDispatchProps = (dispatch, ownProps) => ({
})

export default connect(mapStateToProps, mapToDispatchProps)(MainContainer);

