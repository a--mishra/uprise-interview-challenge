import React from 'react';
import { emphasize, withStyles } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';

import HomeIcon from '@material-ui/icons/Home';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TableChartIcon from '@material-ui/icons/TableChart';
import InsertChartIcon from '@material-ui/icons/InsertChart';

const StyledBreadcrumb = withStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.grey[100],
    height: theme.spacing(3),
    color: theme.palette.grey[800],
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover, &:focus': {
      backgroundColor: theme.palette.grey[300],
    },
    '&:active': {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(theme.palette.grey[300], 0.12),
    },
  },
}))(Chip); // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

function handleClick(event) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

export default function CustomizedBreadcrumbs(props) {

   let tabs = []
   try{
       tabs = props.tabs;
       alert(JSON.stringify(tabs))
   } catch(e){
       // do nothing
   }

  return (

    <Breadcrumbs aria-label="breadcrumb">

        {tabs.map((key, index)=>
                <StyledBreadcrumb
                component="a"
                label={tabs[index].label}
                id={tabs[index].id}
                icon={tabs[index].icon}
                onClick={(event)=>{
                    tabs[index].callback(tabs[index].id)
                }}
            />
        )}

    </Breadcrumbs>
  );
}