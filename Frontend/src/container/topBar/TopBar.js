import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Profile from '../../container/profile/Profile'
import Playlists from '../../container/playlists/Playlists'
import { Button } from '@uprise/button'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Fade from '@material-ui/core/Fade';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import { withStyles } from '@material-ui/core/styles';



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={1}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 0,
    borderRadius: '8px',
    backgroundColor: theme.palette.background.paper,
    // boxShadow: "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)"
    boxShadow:"0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)"
  },
  tab: {
    fontWeight: 'bold',
    color: '#8A69FC',
    fontSize: '0.8rem',
    textTransform: "initial"
  },

  tabIndicator: {
      background: 'transparent'
  }, 

  tabPanelRoot : {
    padding: "0px",
    marginTop: "28px",
    borderRadius: "8px",
    box:{
      padding:"0px"
    }
  }
}));


const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      // backgroundColor: theme.palette.primary.main, #8A69FC
      backgroundColor: '#8A69FC',
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);



export default function SimpleTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let artistsData = props.data.tab3.artists;
  
  let getRandomElemetsFromArray = (arr, count) => {
    var shuffled = arr.slice(0), i = arr.length, min = i - count, temp, index;
    while (i-- > min) {
        index = Math.floor((i + 1) * Math.random());
        temp = shuffled[index];
        shuffled[index] = shuffled[i];
        shuffled[i] = temp;
    }
    return shuffled.slice(min);
  }

  const [artistMenuItem, setArtistMenuItem] = React.useState(getRandomElemetsFromArray(artistsData,10))

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    // get 10 random names from artistsData
    setArtistMenuItem(getRandomElemetsFromArray(artistsData,10))
  };

  const handleClose = () => {
    setAnchorEl(null);
  };



  return (
    <div>
      <AppBar position="static" className={classes.root}>
        <Tabs classes={{ indicator: classes.tabIndicator}} value={value} onChange={handleChange} aria-label="Tabbed Navigation Quota Management Module" direction={'right'} >
          <Tab className={classes.tab} label="Overview" {...a11yProps(0)} />
          <Tab className={classes.tab} label="Playlist" {...a11yProps(1)} />
          <Tab className={classes.tab} label="Featured" {...a11yProps(1)} onClick={handleClick}/>

          <StyledMenu
            id="customized-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            >
              {/* <StyledMenuItem>
                <ListItemIcon>
                  <DraftsIcon fontSize="small" />
                </ListItemIcon>
                <ListItemText primary="Sent mail" />
              </StyledMenuItem> */}

              {artistMenuItem.map(name => (
                <StyledMenuItem>
                    <ListItemText primary={name} />
                </StyledMenuItem>
              ))}

          </StyledMenu>

        </Tabs>
      </AppBar>
      <TabPanel className={'tabPanelWrapper'} value={value} index={0}>
        <Paper className={'tabPanelContainer'}>
          <Profile imgUrl={props.data.tab1.imgUrl} displayName={props.data.tab1.displayName} followersCount={props.data.tab1.followersCount} followCurrentProfile={()=>{props.data.tab1.followCurrentProfile()}}/>
        </Paper>
      </TabPanel>
      <TabPanel className={'tabPanelWrapper'} value={value} index={1}>
        <Paper className={'tabPanelContainer'}>
          <Playlists images={props.data.tab2.images} />
        </Paper>
      </TabPanel>
      <TabPanel className={'tabPanelWrapper noDisplay'} value={value} index={2}>
        <Paper className={'tabPanelContainer'}>

        </Paper>
      </TabPanel>
    </div>
  );
}