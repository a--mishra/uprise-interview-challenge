import React from 'react';
import { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import { fade, makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';

import Tooltip from '@material-ui/core/Tooltip';
import DialpadIcon from '@material-ui/icons/Dialpad';
import PhoneForwardedIcon from '@material-ui/icons/PhoneForwarded';
import VoicemailIcon from '@material-ui/icons/Voicemail';
import GroupIcon from '@material-ui/icons/Group';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import ScheduleIcon from '@material-ui/icons/Schedule';
import EventIcon from '@material-ui/icons/Event';

import {CustomIconButton} from '../widget/IconButton'

import DeleteButton from '../../container/Designer/actionButtons/DeleteNode';
import AddNode from '../../container/Designer/actionButtons/AddNode';
import EditNode from '../../container/Designer/actionButtons/EditNode';

const useStyles1 = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});


var tableCustomHeight = 0;
var innerHeight = window.innerHeight;
const findheight = () => {
  setTimeout(() => {

    let footerHeight = 55;
    const topHeight = document.getElementById('tableheight').getBoundingClientRect().top;
    tableCustomHeight = innerHeight - (topHeight + footerHeight);
    document.getElementById('tableheight').style.height = tableCustomHeight + "px";

  }, 0)

}

const useStyles2 = makeStyles(theme => ({
  root: {
    width: '100%',
    boxShadow: 'none',
    boxShadow: 'inset 0px 0px 16px -8px #e0e0e0'

  },
  tableWrapper: {
    height: '100%',
    overflow: 'auto',
  },
  tablehead: {
    height: 36
  },

  tableRow: {
    height: 36,
    padding: '0 20px 0 20px',
    lineHeight: '14px',
    fontSize: 12,
    color: '#333333',
  },
  headerColor: {
    backgroundColor: '#182C4C',
    color: '#F2F2F2',
    fontSize: '12px',
    fontWeight: 700,
    padding: '10px 20px 10px 20px',
    lineHeight: '15px'
  },
  emptydata: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    fontSize: 12
  },
  pagination: {
    border: '1px solid #cccccc',
    boxShadow: '0 -2px 4px 0 #cccccc',
    minHeight: '60'
  },
  mainheader: {
    borderTop: '1px solid #cccccc'
  },
  searchbox: {
    margin: "10px 0 10px 20px"
  },

  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    display: 'inline-block',
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },

    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  svgsize: {
    // width: theme.spacing(7),
    // height: '100%',
    // position: 'absolute',
    // pointerEvents: 'none',
    // display: 'flex',
    // alignItems: 'center',
    // justifyContent: 'center',
    // right:'0',
    '& > svg': {
      height: 9,
      width: 9
    },
  },
  searchIcon: {
    display: 'inline-block',
    border: '1px solid #ccc',
    borderRadius: 25,
  },

  inputRoot: {
    border: '1px solid #cccccc',
    borderRadius: '25px'
  },
  inputInput: {
    padding: theme.spacing(1, 7, 1, 1),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },

  lebelsize: {
    fontSize: 10
  },
  serchbutton: {
    width: 160,
    height: 24,
    fontSize: 10
  },
  textfildfontsize: {
    '& ..MuiInputBase-input': {
      fontSize: 10
    },
  },
  levelIconWrapper:{
  }

}));



function Row(props) {
  let { row, columns, isLast, isFirst } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useStyles2();

  row.action = row.type == 'Menu' ?
  <> 
      <AddNode id={row.id} callback={(id,data) => {props.addNode(id, data)}} ></AddNode>
      <EditNode id={row.id} callback={(id,data) => {props.editNode(id, data)}} row={row} ></EditNode>
      <DeleteButton id={row.id} callback={(id) => {props.deleteNode(id)}} ></DeleteButton>
  </> : 
  <>
      <EditNode id={row.id} callback={(id,data) => {props.editNode(id, data)}} row={row}></EditNode>
      <DeleteButton id={row.id} callback={(id) => {props.deleteNode(id)}} ></DeleteButton>
  </>;

  switch (row.type) {
    case 'Menu':
        row.typeIcon = <Tooltip title="Menu">
                                <IconButton aria-label="Menu">
                                <DialpadIcon />
                                </IconButton>
                            </Tooltip>
        break;

        case 'Phone':
            row.typeIcon = <Tooltip title="Phone">
                                    <IconButton aria-label="Phone">
                                    <PhoneForwardedIcon />
                                    </IconButton>
                                </Tooltip>
            break;

            case 'VoiceMail':
                row.typeIcon = <Tooltip title="VoiceMail">
                                        <IconButton aria-label="VoiceMail">
                                        <VoicemailIcon />
                                        </IconButton>
                                    </Tooltip>
                break;

                case 'Sound':
                    row.typeIcon = <Tooltip title="Sound">
                                            <IconButton aria-label="Sound">
                                            <RecordVoiceOverIcon />
                                            </IconButton>
                                        </Tooltip>
                    break;

                    case 'Group':
                        row.typeIcon = <Tooltip title="Group">
                                                <IconButton aria-label="Group">
                                                <GroupIcon />
                                                </IconButton>
                                            </Tooltip>
                        break;

                        case 'OfficeHours':
                            row.typeIcon = <Tooltip title="OfficeHours">
                                                    <IconButton aria-label="OfficeHours">
                                                    <ScheduleIcon />
                                                    </IconButton>
                                                </Tooltip>
                            break;


                        case 'Holiday':
                            row.typeIcon = <Tooltip title="Holiday">
                                                    <IconButton aria-label="Holiday">
                                                    <EventIcon />
                                                    </IconButton>
                                                </Tooltip>
                            break;

    default: 
        row.typeIcon = <Tooltip title="---">
                                    <IconButton aria-label="---">
                                    </IconButton>
                                </Tooltip>
        break;
  }

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          {row.children && row.children.length > 0 ? <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton> : <><div style={{height:27}}></div></>}
        </TableCell>
        {columns.map(column => {
          const value = row[column.id];
          if(column.id == 'id') {
            return (<></>)
          }
          return (
            <TableCell 
              component="th" 
              scope="row"
              key={column.id}
              align={column.align}
              style={{ minWidth: column.id =='id' ? 1 : column.id =='level' ? 2 : column.minWidth, width: column.id =='id' || column.id =='level' ? '10px' : column.id =='level' ? '36px' : 'auto' }}
              className={classes.tableRow}>
              {value}
            </TableCell>
          );
        })}
        <div style={{
            position: 'relative',
            right: '36px',
            top: '8px',
            width: '0px'
          }}>
          <div style={{
            width: '21px',
            height: '21px'
            }}>
            <IconButton aria-label="expand row" size="small" onClick={() => {props.moveUp(row.id)}}>
            {isFirst ? <></> : <KeyboardArrowUpIcon />}
            </IconButton>
          </div>
          <div style={{
            width: '21px',
            height: '21px'
            }}>
            <IconButton aria-label="expand row" size="small" onClick={() => {props.moveDown(row.id)}}>
            {isLast ? <></> : <KeyboardArrowDownIcon />}
            </IconButton>
          </div>
        </div>
      </TableRow>

      {row.children && row.children.length > 0 ? <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 , paddingRight: 0, paddingLeft: 0, boxShadow: 'inset 0px 0px 16px -8px #e0e0e0'}} colSpan={columns.length}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1} style={{ paddingBottom: 2, paddingTop: 2 , paddingRight: 2, paddingLeft: 2}}>
              <Typography variant="h6" gutterBottom component="div">
                
              </Typography>

              <CollapsibleTable tableData={row.children}  tableHeader={window.TableHeaders(row.type)} noSelectionFallback={false} addNode={(id,data) => {props.addNode(id, data)}} deleteNode={(id) => {props.deleteNode(id)}}  editNode={(id,data) => {props.editNode(id, data)}} moveUp={(id)=>{props.moveUp(id)}} moveDown={(id)=>{props.moveDown(id)}} />

            </Box>
          </Collapse>
        </TableCell>
      </TableRow> : <></>}
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    // calories: PropTypes.number.isRequired,
    // carbs: PropTypes.number.isRequired,
    // fat: PropTypes.number.isRequired,
    // history: PropTypes.arrayOf(
    //   PropTypes.shape({
    //     amount: PropTypes.number.isRequired,
    //     customerId: PropTypes.string.isRequired,
    //     date: PropTypes.string.isRequired,
    //   }),
    // ).isRequired,
    // name: PropTypes.string.isRequired,
    // price: PropTypes.number.isRequired,
    // protein: PropTypes.number.isRequired,
  }).isRequired,
};


export default function CollapsibleTable(props) {
  const classes = useStyles2();

  const [tableData, setTableData] = React.useState([]);
  useEffect(
    () => {
        setTableData(props.tableData);
    },[props.tableData]
  );

  const columns = props.tableHeader;

  let isLast = false;
  let isFirst = false;
  if(props.selectedIVR != undefined || props.noSelectionFallback == false){
    if ( tableData && tableData.length !== 0) {
      return (
        <TableContainer component={Paper}>
        <div className={classes.tableWrapper} id="tableheight">

          <Table aria-label="collapsible table">
            <TableHead className={classes.tablehead}>
              <TableRow>
                {columns.map(column => (
                  <TableCell className={classes.headerColor}
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.id =='id'  ? 1 : column.id =='level' ? 2: column.minWidth, width: column.id =='id' ? '10px' : column.id =='level' ? '36px': 'auto' }}
                  >
                    {column.id == 'id' ?
                      <div className={classes.levelIconWrapper}>
                        <LevelIcon tableData={tableData}/>
                      </div> : 
                      column.label}
                  </TableCell>
                ))}
                <div style={{
                  position: 'relative',
                  right: '36px',
                  top: '8px',
                  width: '0px'
                }}>
                <div style={{
                  width: '21px',
                  height: '21px'
                  }}>
                  <Level0AddIcon tableData={tableData} icon={props.level0AddIcon}/>
                </div>
              </div>
              </TableRow>
            </TableHead>
            <TableBody>
              {tableData.map((row, index) => (
                <Row key={row.id} row={row} columns={columns} isFirst={index == 0 ? true : false} isLast={index == tableData.length -1 ? true : false} addNode={(id,data) => {props.addNode(id, data)}} deleteNode={(id) => {props.deleteNode(id)}} editNode={(id,data) => {props.editNode(id, data)}} moveUp={(id)=>{props.moveUp(id)}} moveDown={(id)=>{props.moveDown(id)}} />
              ))}
            </TableBody>
          </Table>

          </div>

        </TableContainer>
      );
    }
    else {
      return (
        <Paper className={classes.root}>
          <div className={classes.tableWrapper}>
            <div className={classes.emptydata}>
              <p> No Record to display, please create one by hitting the {props.level0AddIcon}  button!</p>
            </div>
          </div>
        </Paper>
      )
    }
  } 
  else {
    return (
      <Paper className={classes.root}>
        <div className={classes.tableWrapper}>
          <div className={classes.emptydata}>
            <p> Select a flow to modify, or hit the {props.createNewIVRIcon} button to create one!</p>
          </div>
        </div>
      </Paper>
    )
  }
}


function LevelIcon(props) {
  let { level, tableData } = props;
  
  try{
    level = tableData[0].level;
  } catch(e) {
    // do nothing
  }

  return (
    <React.Fragment>
    {level == undefined || level == null ? <CustomIconButton variant="contained"></CustomIconButton>  : <CustomIconButton variant="contained">{level}</CustomIconButton>
      }
    </React.Fragment>
  );
}

function Level0AddIcon(props) {
  let { level, tableData } = props;
  
  try{
    level = tableData[0].level;
  } catch(e) {
    // do nothing
  }

  let icon = props.icon
  return (
    <React.Fragment>
    {level == 1 ? icon : <></>}
    </React.Fragment>
  );
}