import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { Button } from '@uprise/button'


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1    
  },
  image: {
    // width: 256,
    height: 232,
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
    borderRadius: '8px'
  },
}));

export default function Profile(props) {
  const classes = useStyles();

  return (
    <div className={classes.root} className={'profileContainer'}>
        <Grid container spacing={4}>
          <Grid item>
            <ButtonBase className={classes.image}>
              <img className={classes.img} alt="complex" src={props.imgUrl ? props.imgUrl : "https://i.scdn.co/image/ab6775700000ee85cbad6f248c4cc48a076d39a7"}/>
            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction="column" spacing={2}>
              <Grid item xs>
                <Typography gutterBottom variant="h3">
                  {props.displayName ? props.displayName : 'ChilledCow'}
                </Typography>
                <Typography gutterBottom variant="subtitle1" color="textSecondary">
                  Followers ( {props.followersCount ? props.followersCount : '10,949,948'} )
                </Typography>
              </Grid>
              <Grid item>
                <Grid item xs={12} sm={12} md={9} lg={4} container>
                  <Button onClick={()=>{props.followCurrentProfile()}} title={'Follow'} className={'followButton'}/>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
    </div>
  );
}
