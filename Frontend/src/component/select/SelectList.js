import React, { Component } from 'react'
import { connect } from 'react-redux'
import {withStyles} from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const styles = theme => ({

  formControl: {
    minWidth: 215, 
    height:35
  },
  option: {
    fontSize:12
  },
  
  setBorder: {
    height:62,
    marginLeft:20
  },
  labelsize: {
      fontSize:10
  },
  optionsize: {
    fontSize: 12
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  selectheight: {
    height:62,
  },
  optionHeight: {
    margin:5
  }  
});


class SelectList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      options: [],
      selectedOption:''
    };
  }

  componentWillMount(){

  }

  onSelectChange = (event, values) => {

    if(values != null) {
      this.props.callback(values.id);
    }
  
  } 


  render() {
    const {classes} = this.props;

    return (
      <div>
        <div className={classes.setBorder} >
            <FormControl className={classes.formControl}>
              <Autocomplete
                options= {this.props.optionsList}
                getOptionLabel= {option => option.title}
                // id="clear-on-escape"
                id="disable-clearable"
                className="selectCampaign"
                classes={{
                  option: classes.option,
                }}
                disableClearable
                onChange={(event,value)=>{this.onSelectChange(event, value)}}
                renderInput={params => (
                  <TextField {...params} label={this.props.label}  margin="normal" fullWidth  />
                )}
              />
            </FormControl>
          </div>
      </div>      
    );
  }
}


const mapStateToProps = (state) => ({
})

const mapToDispatchProps = (dispatch, ownProps) => ({
})



export default connect(mapStateToProps, mapToDispatchProps)(withStyles(styles)(SelectList));