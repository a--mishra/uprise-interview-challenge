import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'

import {rootReducer} from '../reducers/rootReducer'
//import { browserHistory} from 'react-router'  depricated from react-router@4 onwards
//import {BrowserRouter} from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import { routerMiddleware} from 'react-router-redux'
import { devToolsEnhancer,composeWithDevTools } from 'redux-devtools-extension'


//import { reducer as formReducer } from 'redux-form';


//const middleware = routerMiddleware(BrowserRouter)
const composeEnhancers = ''//window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;


const configureStore = preloadedState => {
    const store = createStore(
                                rootReducer,
                               // formReducer,
                                preloadedState,
                                compose(
                                    composeWithDevTools(applyMiddleware(thunk,createLogger) )
                                    //composeWithDevTools({trace: true, traceLimit: 25 })
                                    //applyMiddleware(thunk,createLogger)
                                    //devToolsEnhancer()
                                )
                            )
    return store
}
export default configureStore

