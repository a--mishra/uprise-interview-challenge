import React from 'react';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import PopupState, { bindTrigger, bindPopover } from 'material-ui-popup-state';
import { CustomIconButton } from '../widget/IconButton';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import FacebookIcon from '@material-ui/icons/Facebook';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import EmailIcon from '@material-ui/icons/Email';
import { Grid } from '@material-ui/core';
// import Mailto from 'react-mailto'
// var Mailto = require('react-mailto');

const styleForIcon = {
  display: 'inline',
  position: 'relative',
  top: '6px'
}

const headersForMailTo = {
  email:"ashutoshmishra@ameyo.com",
  subject:"In Regards to ",
  cc:"ashutosh.m812@gmail.com",
  body:"Triggered from appInfoPopUp "
}

export default function AppInfoPopUp(props) {
  window.log(props, 'props in appInfo', 'Debug', 2)
  return (
    <PopupState variant="popover" popupId="demo-popup-popover">
      {(popupState) => (
        <div>
          {/* <Button variant="contained" color="primary" {...bindTrigger(popupState)}>
            Open Popover
          </Button> */}
          <CustomIconButton variant="contained" {...bindTrigger(popupState)} ><InfoOutlinedIcon/></CustomIconButton>

          <Popover
            {...bindPopover(popupState)}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            <Box p={2}>
              {/* <Typography>{props.configuration.name}</Typography>
              <Typography>{props.configuration.version}</Typography>
              <Typography>{props.configuration.author } <LinkedInIcon/> </Typography> */}

              <Typography variant="button" display="block" gutterBottom>
                {props.configuration.name}
              </Typography>
              <Typography variant="caption" display="block" gutterBottom>
                {props.configuration.version}
              </Typography>
              <Typography variant="overline" display="block" gutterBottom>
                {`${props.configuration.author} | `}
                <div style={styleForIcon}>
                    <a href={`mailto: ${headersForMailTo.email}?subject=${headersForMailTo.subject} ${props.configuration.name} | ${props.configuration.version}&cc=${headersForMailTo.cc}&body=${headersForMailTo.body}`}><EmailIcon/></a>
                </div>
              </Typography>
            </Box>
          </Popover>
        </div>
      )}
    </PopupState>
  );
}