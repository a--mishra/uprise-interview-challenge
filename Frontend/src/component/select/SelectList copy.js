import React, { Component } from 'react'
import { connect } from 'react-redux'
import {withStyles} from '@material-ui/core/styles';

import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

const styles = theme => ({

  formControl: {
    minWidth: 215, 
    height:35
  },
  option: {
    fontSize:12
  },
  
  setBorder: {
    height:62,
    marginLeft:20
  },
  labelsize: {
      fontSize:10
  },
  optionsize: {
    fontSize: 12
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  selectheight: {
    height:62,
  },
  optionHeight: {
    margin:5
  }  
});


class SelectList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      options: [],
      selectedOption:''
    };
  }

  componentWillMount(){

  }

  onSelectChange = (event, values) => {

    if(values != null) {
      this.props.callback(values.id);
    }
  
  } 


  render() {
    const {classes} = this.props;

    return (
      <div>
        <div className={classes.setBorder} >
          <FormControl className={classes.formControl}>
    <InputLabel id="grouped-select-label">{this.props.label}</InputLabel>
            <Select
              labelId="grouped-select-label"
              id="grouped-select"
              multiple
              value={personName}
              onChange={this.props.callback}
              input={<Input />}
              renderValue={(selected) => selected.join(', ')}
              MenuProps={MenuProps}
            >
              {names.map((name) => (
                <MenuItem key={name} value={name}>
                  <Checkbox checked={personName.indexOf(name) > -1} />
                  <ListItemText primary={name} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
    </div>      
    );
  }
}


const mapStateToProps = (state) => ({
})

const mapToDispatchProps = (dispatch, ownProps) => ({
})



export default connect(mapStateToProps, mapToDispatchProps)(withStyles(styles)(SelectList));