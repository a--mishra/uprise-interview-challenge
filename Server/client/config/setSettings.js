// TODO: check for locale
var globalSettings = {};

var request = new XMLHttpRequest();
request.open('GET', `./config/settings.json`, false);  // `false` makes the request synchronous
request.send(null);

if (request.status === 200) {
    globalSettings = JSON.parse(request.responseText);
    log(globalSettings, "globalSettings", 'Debug',1);
} else {
    globalSettings = {};
    log(globalSettings, "Failed to get globalSettings set with Defaults", 'Debug', 1);
}